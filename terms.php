  <section class="policy">
    <h2>Terms and Conditions</h2>
    <hr>
    <h3>Table of Contents</h3>
    <!-- Table of contents -->
    <ol>
      <li><a href="#definitions">Definitions</a></li>
      <li><a href="#services">Products &amp; Services</a></li>
      <li><a href="#prices">Prices &amp; Payment</a></li>
      <li><a href="#guarentees">Guarentees &amp; Warranties</a></li>
      <li><a href="#copyright">Copyright &amp; Trademarks</a></li>
      <li><a href="#termination">Termination of Service</a></li>
      <li><a href="#"></a></li>
      <li><a href="#liability">Liability</a></li>
      <li><a href="#changes">Changes to Terms</a></li>
    </ol>
    <!-- Definitions -->
    <section id="definitions">
      <h3>Definitions</h3>
      <hr>
      <p>The term, "Klon" is used throughout this document. The term "Klon" in this context refers to "Klon LLC", a legally registered LLC in the State of Ohio, see <a href="https://bizimage.sos.state.oh.us/api/image/pdf/201908102102">Document No. 201908102102.</a></p>
      <p>"Klon Privacy Extension" refers to the browser extension offered as a service by Klon</p>
      <p>"Klon Privacy Extension API" refers to the underlying API behind Klon Privacy Extension. The API is the means by which Klon Privacy Extension communicates back and forth to the main server.</p>
      <p>"Server" refers to any number of servers used by Klon, Klon Privacy Extension, and or Klon Privacy Extension API such as web server(s), database server(s), etc...</p>
      <p>"Client(s)", "User(s)" refers to the entities which use Klon Privacy Extension.</p>
      <p>"Our" refers to anything owned by Klon.</p>
      <p>"Month" refers to a period of 30 days.</p>
      <p></p>
    </section>
    <section>
      <h3>Accepting the Terms</h3>
      <hr>
      <p>By using the Service, you agree to be bound by this Agreement, whether you are a “Visitor” (which means that you simply browse our websites or use our software without registering) or you are a “User” (which means that you have registered to create an account with Klon). The term “you” refers to a Visitor or a User. The term “we” or “our” refers to Klon LLC. You may not use the Service and you may not accept this Agreement if you are not of a legal age to form a binding contract with Klon. If you accept this Agreement, you represent that you have the capacity to be bound by it or if you are acting on behalf of a company or entity that you have the authority to bind such entity. If you do not agree to this Agreement, please don’t use the Service.</p>
      <p>THIS AGREEMENT CONTAINS (1) AN ARBITRATION PROVISION; (2) A WAIVER OF RIGHTS TO BRING A CLASS ACTION AGAINST US; AND (3) A RELEASE BY YOU OF ALL CLAIMS FOR DAMAGE AGAINST US THAT MAY ARISE OUT OF YOUR USE OF THE SERVICE. BY USING THE SERVICE, YOU AFFIRM THAT YOU AGREE TO THESE PROVISIONS.</p>
    </section>
    <section>
      <h3>Your Registration Information</h3>
      <hr>
      <p>You agree and understand that you are responsible for maintaining the confidentiality of your password which, together with your email address, allows you to access the Service. That email address and password, together with any mobile number or other contact information you provide form your “Registration Information.”</p>
      <p>By providing us with your email address, you agree to receive all required notices electronically, to that e-mail address. It is your responsibility to update or change that address, as appropriate. Notices will be provided in HTML (or, if your system does not support HTML, in plain-text) in the text of the e-mail or through a link to the appropriate page on our site, accessible through any standard modern, commercially available internet browser.</p>
      <p>If you become aware of any unauthorized use of your Registration Information, you agree to notify Klon immediately.</p>
    </section>
    <section>
      <h3>Your Use of the Service</h3>
      <p>Your right to access and use the Service is personal to you and is not transferable by you to any other person or entity. You are only entitled to access and use our Service for lawful, internal, and non- commercial purposes. Subject to your compliance with this Agreement, Klon LLC hereby grants to you, a personal, non-assignable, non-sublicensable, non-transferrable, and non-exclusive license to use the software and content provided to you as part of the Services.</p>
    </section>
    <!-- Products and Services -->
    <section id="services">
      <h3>Products &amp; Services</h3>
      <hr>
      <h4>This section will outline the products and services offered by Klon LLC. This section will also attempt to define the proper use of the products and services offered by Klon LLC.</h4>
      <p>As of May 9, 2019 Klon offers one service known as "Klon Privacy Extension". This service integrates with our proprietary API known as Klon Privacy Extension API.</p>
      <p>Klon Privacy Extension is meant for the sole purpose of allowing our clients to signup and login to their favorite websites easily using fictional credentials. This service provides the client the ability to use their favorite website while not exposing their personal information. Each user has their own private cryptographic key assigned to them. With this key all of their saved identities are encrypted and uploaded to our server for storage. Nobody with access to our database can read any important information stored without this unique key. We encrypt every bit of our generated identities with exception to the email address and corresponding id number. Klon Privacy Extension has a built in email service. Every email that we store on our server is encrypted and stored for a maximum of 72 hours in our database and is then deleted unless the client opts to delete the message themselves. The backbone of our email service is provided by Mailgun. By using our service you must also agree to the terms set forth by Mailgun. You can read their terms and policies here: <a href="https://www.mailgun.com/privacy-policy">Privacy Policy</a> | <a href="https://www.mailgun.com/terms">Terms of Service</a> | <a href="https://www.mailgun.com/gdpr">GDPR Compliance</a> After a message is deleted there is no way for any member of Klon to retrieve it. Klon also offers a password generator contained inside the Klon Privacy Extension. The password generator will generate a password for the client upon request with a metered security rating based on the library "zxcvbn.js" by Dropbox. This library provides only an estimation of password strength and is NOT meant to provide any sort of warranty or guarentee by Klon, Klon Privacy Extension, or the Klon Privacy Extension API with regards to password security or anyones ability to crack or brute force the password. This is covered more in depth in the guarentees &amp; warranty section of this document.</p>
    </section>
    <!-- Prices and Payment-->
    <section id="prices">
      <h3>Prices &amp; Payment</h3>
      <hr>
      <p>The prices and payment for the service(s) offered by Klon are offered in the table below and are subject to availability. We reserve the right to change prices and methods of payment without notice at any time. Any change in price will not effect a current billing cycle. Example: If you started a month long billing cycle on June 1, 2019 and prices went up on June 2, 2019 you will not see an additional bill for more money until the following month, likewise there will be no refunds should the price decrease. You agree to pay Klon the amount for which you sign up for within 24 hours of signing up for the service. Chargebacks or failure to make payments may result in increasing fees. You agree to pay all fees, taxes, and other charges Klon may place on your account for any reason. If payment is not made within 24 hours of signing up for our service(s) we reserve the right to terminate your account.</p>
      <table>
        <tr>
          <th></th>
          <th>Trial Account</th>
          <th>Basic Account</th>
          <th>Regular Account</th>
          <th>Premium Account</th>
        </tr>
        <tr>
          <td>Price</td>
          <td>FREE for 30 days</td>
          <td>$2.99/Month</td>
          <td>$4.99/Month</td>
          <td>$8.99/Month</td>
        </tr>
      </table>
    </section>
    <!-- Guarentees & Warranties -->
    <section id="guarentees">
      <h3>Guarentees &amp; Warranties</h3>
      <hr>
      <p>Klon and all of its members make absolutely NO guarentee to the availability, reliability, or effectiveness of the service provided. We provide absolutely NO warranty and NO guarentee with our service. By registering an account with Klon either via Klon Privacy Extension, Klon Privacy Extension API, and or Klon.io you acknowledge that you have read the these terms and conditions in their entirety and you agree to the terms put forward throughout the entirety of this document. You also agree that you nor any council summoned by you or your acquaintances will hold Klon or any member, owner, share holder, or associate of Klon liable for any losses you or your business may have incurred while using, ANY of our products or services. This service is NOT intended to assist anyone in the commission of fraudulent or illegal activity. You agree that you will ALWAYS use your real government issued identity where it is legally required that you do so. Any attempt to break the law using our service will result in permanent ban from the Klon platform. It is your responsibility to understand the law(s) in your area. We take absolutely NO responsibility for your failure to comply with local, state, federal, or international law. If you use our service to send spam email you will be permanently banned without refund. If we get any complaints of your Klon email address(es) sending out spam your account will be terminated and you will not receive a refund.</p>
    </section>
    <!--  Trademark and Copyright -->
    <section id="copyright">
      <h3>Trademark &amp; Copyright</h3>
      <hr>
      <p>Klon LLC reserves the name Klon LLC in the State of Ohio. We also go by the name "Klon" or "Klon Privacy". The Klon logo shall not be reproduced or used anywhere without the written consent of a Klon manager. Klon reserves the right to the markup, code, styling, graphics, and all other aspects that make up of our service.</p>
    </section>
    <!-- Termination of Service -->
    <section id="termination">
      <h3>Termination of Service</h3>
      <hr>
      <p>Klon reserves the right to terminate any account at any time without warning or explanation for any reason. The most common reasons a user will have their account terminated are due to instances of spam or fraud. If your account is terminated you agree to forfeit any refund to Klon as reconciliation for damages. These charges will come from bandwidth used, storage space used, time spent mitigating the problem, and other related expenses. <strong>Klon will issue absolutely NO REFUNDS to users whom are banned for but not limited to: abusing our system(s), abusing our service(s), or breaking the law while using our service.</strong></p>
    </section>
    <!-- Liability -->
    <section id="liability">
      <h3>Limitations &amp; Liability</h3>
      <hr>
      <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE CONTENT AND ALL SERVICES AND PRODUCTS ASSOCIATED WITH THE SERVICE OR PROVIDED THROUGH THE SERVICE (WHETHER OR NOT SPONSORED) ARE PROVIDED TO YOU ON AN “AS-IS” AND “AS AVAILABLE” BASIS. KLON MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE CONTENT OR OPERATION OF THE SERVICE OR OF THE SERVICE. YOU EXPRESSLY AGREE THAT YOUR USE OF THE SERVICE IS AT YOUR SOLE RISK.
      <p>KLON MAKES NO REPRESENTATIONS, WARRANTIES OR GUARANTEES, EXPRESS OR IMPLIED, REGARDING THE ACCURACY, RELIABILITY OR COMPLETENESS OF THE CONTENT ON THE SERVICE OR OF THE SERVICE (WHETHER OR NOT SPONSORED), AND EXPRESSLY DISCLAIMS ANY WARRANTIES OF NON-INFRINGEMENT OR FITNESS FOR A PARTICULAR PURPOSE. KLON MAKES NO REPRESENTATION, WARRANTY OR GUARANTEE THAT THE CONTENT THAT MAY BE AVAILABLE THROUGH THE SERVICE IS FREE OF INFECTION FROM ANY VIRUSES OR OTHER CODE OR COMPUTER PROGRAMMING ROUTINES THAT CONTAIN CONTAMINATING OR DESTRUCTIVE PROPERTIES OR THAT ARE INTENDED TO DAMAGE, SURREPTITIOUSLY INTERCEPT OR EXPROPRIATE ANY SYSTEM, DATA OR PERSONAL INFORMATION. YOU ARE RESPONSIBLE FOR ENSURING THE ACCURACY OF ANY USER CONTENT THAT YOU PROVIDE, AND WE DISCLAIM ALL LIABILITY AND RESPONSIBILITY FOR THE ACCURACY OF SUCH CONTENT.</p>
      <p>KLON SHALL IN NO EVENT BE RESPONSIBLE OR LIABLE TO YOU OR TO ANY THIRD PARTY, WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY, LIQUIDATED OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO LOSS OF PROFIT, REVENUE OR BUSINESS, ARISING IN WHOLE OR IN PART FROM YOUR ACCESS TO THE SERVICE, YOUR USE OF THE SERVICE OR THIS AGREEMENT, OR FOR ANY LOSS OR DAMAGE CAUSED BY YOUR RELIANCE ON INFORMATION OBTAINED ON OR THROUGH THE SERVICE, EVEN IF KLON HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY IN THIS AGREEMENT, KLON’S LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO $500.00 (FIVE HUNDRED UNITED STATES DOLLARS).</p>
      <p>BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, THE LIABILITY OF KLON SHALL BE LIMITED TO THE FULLEST EXTENT PERMITTED BY LAW. THEREFORE, THE FOREGOING LIMITATIONS SHALL APPLY TO THE MAXIMUM EXTENT PERMITTED BY LAW. NOTHING IN THIS AGREEMENT AFFECTS STATUTORY RIGHTS THAT CANNOT BE WAIVED OR LIMITED BY CONTRACT.</p>
      <p>Klon takes NO responsibility for what our users do with the service provided by Klon. Klon will not be held liable for any of our users misuse, abuse, or illegal activities resulting from use of our service. Any abuse that has sufficient enough evidence for us to determine as innapropriate and/or illegal will result in a permanent ban of the user conducting the abusive and or illegal behavior. We are NOT trying to create a haven for identity thieves or any other sort illegal activity. <strong>KLON ABSOLUTELY WILL NOT TOLERATE ANY ABUSE OF OUR SERVICE(S).</strong> The identities created by the Klon Privacy Extension are completely fictional. Klon will not be held liable for any passwords that are compromised in any way via the password generator tool provided in the Klon Privacy Extension. The security of the passwords generated there are estimated to be secure by the library provided by Dropbox and are NOT guarenteed in any way.</p>
      <p>You shall defend, indemnify and hold harmless Klon and its members, officers, directors, shareholders, and employees, from and against all claims and expenses, including but not limited to attorneys fees, in whole or in part arising out of or attributable to your use of the Service (including any purchases you make within the Klon Privacy Extension) or any breach of this Agreement by you or your violation of any law or the rights of a third party when using the Service.</p>
    </section>
    <section>
      <h4>Ending your Relationship with Klon</h4>
      <hr>
      <p>This Agreement will continue to apply until terminated by either you or Klon as set out below. If you want to terminate your legal agreement with Klon, you may do so by closing your account for the Service.</p>
      <p>Please use the directions below to cancel your account, only if you have created a Membership. Otherwise just uninstall Klon and stop visiting our Website and using other Services.</p>
      <p>Email support@klon.io to cancel your account. Upon doing so: your account will be closed, your Klon subscription will be set to cancel at the end of the month, and your ability to log in deactivated; and any data in our records will be retained subject to our Privacy Policy.<p>
      <p>Klon may at any time, terminate its legal agreement with you:</p>
      <p>if you have breached any provision of this Agreement (or have acted in a manner which clearly shows that you do not intend to, or are unable to comply with the provisions of this Agreement);</p>
      <p>if Klon in its sole discretion believes it is required to do so by law (for example, where the provision of the Service to you is, or becomes, unlawful); or</p>
      <p>immediately upon notice, to the e-mail address provided by you as part of your Registration Information.</p>
    </section>
    <!-- Changes -->
    <section id="changes">
      <h3>Changes to Terms</h3>
      <hr>
      <p>You agree to keep yourself up-to-date on the changes regarding our Terms and Conditions. Klon reserves the right to make changes to our Terms and Conditions at any time for any reason. We will attempt to notify you of any changes to our Terms and Conditions via the email address associated with your account 30 days prior to the changes taking effect. By continuing to use this service you are agreeing to the Terms and Conditions contained in this document in their entirety. You agree not to use any service provided by Klon, Klon Privacy Extension, or Klon Privacy Extension API for illegal or abusive behavior. You also agree that you are authorized to use these services by Klon. You agree that you have not nor have ever been banned from using Klon services in the past. You agree and acknowledge that repeated misuse or abuse of our service(s) or system(s) may result in legal consequences. Klon reserves the right to recover any losses incurred by its users through abuse, misuse, illegal activity, or any other illicit activity through legal action and or banning from our service(s) without refund.</p>
      <p>Klon may modify this Agreement from time to time. Any and all changes to this Agreement will be posted on the Website(s). In addition, the Agreement will always indicate the date it was last revised. You are deemed to accept and agree to be bound by any changes to the Agreement when you use the Service after those changes are posted.</p>
    </section>
    <section>
      <h3 id="misc">Miscellaneous</h3>
      <p>If any portion of this Agreement is deemed unlawful, void or unenforceable by any arbitrator or court of competent jurisdiction, this Agreement as a whole shall not be deemed unlawful, void or unenforceable, but only that portion of this Agreement that is unlawful, void or unenforceable shall be stricken from this Agreement.</p>
      <p>You agree that if Klon does not exercise or enforce any legal right or remedy which is contained in the Agreement (or which Klon has the benefit of under any applicable law), this will not be taken to be a formal waiver of Klon’s rights and that those rights or remedies will still be available to Klon.</p>
      <p>All covenants, agreements, representations and warranties made in this Agreement shall survive your acceptance of this Agreement and the termination of this Agreement.</p>
    </section>
  </section>
