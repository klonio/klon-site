/* Event Scheduler is off by default to use it you have to turn it on */
SET GLOBAL event_scheduler = ON;

/* View processes */
SHOW PROCESSLIST;

/* Schedule the event for every 1 hour */
CREATE EVENT clear_mail
  ON SCHEDULE
    EVERY 1 HOUR
  COMMENT 'Prune old emails'
  DO
  DELETE FROM `mail` WHERE `timestamp` + 259200 < UNIX_TIMESTAMP(NOW());
