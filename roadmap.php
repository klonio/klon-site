<section id="roadmap">
  <h2 class="roadmap__title">Roadmap</h2>
  <div class="roadmap__grid">
    <div class="roadmap__block">
      <div class="roadmap__complete"></div>
      <div class="roadmap__heading">
        <h3>Mozilla Firefox Support</h3>
      </div>
      <div class="roadmap__image">
        <img alt="Firefox Logo" src="./img/roadmap/firefox-logo.svg" height="200" width="200" aria-label="Firefox Logo">
      </div>
      <div class="roadmap__text">This is currently our top priority in terms of development. We want our extension to be available to as many people across as many platforms as possible.</div>
    </div>
    <div class="roadmap__block">
      <div class="roadmap__heading">
        <h3>Klon Lite</h3>
      </div>
      <div class="roadmap__image">
        <img alt="Klon Lite" src="./img/roadmap/klon-lite.svg" height="200" width="200" aria-label="Klon Lite">
      </div>
      <div class="roadmap__text">Klon Lite will be a lite version of our Klon Privacy Extension. The Lite version will primarily be a password manager.</div>
    </div>
    <div class="roadmap__block">
      <div class="roadmap__heading">
        <h3>Improve UI/UX</h3>
      </div>
      <div class="roadmap__image">
        <img alt="UI/UX Improvements" src="./img/roadmap/ui-ux.svg" height="200" width="200" aria-label="UI/UX Improvements">
      </div>
      <div class="roadmap__text">Here at Klon we appreciate great design and usability. After all if our clients can't use our service there's no point in offering it. We want to be sure we are always offering the latest and greatest for our clients and so this is why we are focused on our User Interface (UI) and User Experience (UX).</div>
    </div>
    <div class="roadmap__block">
      <div class="roadmap__heading">
        <h3>SMS Support</h3>
      </div>
      <div class="roadmap__image">
        <img alt="SMS Support" src="./img/roadmap/sms-support.svg" height="200" width="200" aria-label="SMS Support">
      </div>
      <div class="roadmap__text">Sometimes when you sign up for a service they will require a phone number for verification. This is just another example of how businesses are overstepping their bounds under the guise of "convenience". In reality your phone number can be used to target you more deliberately with ads and other nuissance communcation. We'll keep your personal number private by integrating SMS support into Klon.</div>
    </div>
  </div>
</section>
