var modal = {


  /**
   *
   *  @param   id    *Optional*    String     id given to the created modal
   *
   *  @return                      Boolean    true if successful false otherwise
   *
   */
  create : function(id = false) {
    var newModal = document.createElement("div");
    newModal     = document.appendChild(newModal);
    return true;
  },


  /**
   *
   *  @param   id    *Optional*     String     id of the target modal
   *
   *  @return                       Boolean    true if successful false otherwise
   *
   */
  show : function(id = false) {
    if (id === null) {
      return false;
    } else {
      // Get the modal
      var m = document.querySelector("#" + id);
      // Make sure we have a result
      if (m !== null) {
        // Check the classList for modal--hide and remove it
        if (m.classList.contains('modal--hide')) {
          // Remove the hide class
          m.classList.remove('modal--hide');
          return true;
        } else {
          // Already showing
          return true;
        }
      } else {
        // Doesnt exist
        return false;
      }
      return m;
    }
  }
}
