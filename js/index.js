/**
 *  Check if an element is in the visible viewport
 *
 *  @param      el       HTMLElement
 *
 *  @return              Boolean
 *
 */
var IsInViewport = function(el) {
  var rect = el.getBoundingClientRect();
  return (rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth));
};

var videoSignUp   = document.querySelector("#vidSignUp");
var videoGenerate = document.querySelector("#vidGenerate");
var videoFill     = document.querySelector("#vidFill");

var areVideosInView = debounce(function() {
	// All the taxing stuff you do
  if (IsInViewport(videoSignUp)) {
    //Older browsers may not return a promise, according to the MDN website
    var promiseVidSignUp = videoSignUp.play();
    if (promiseVidSignUp) {
        promiseVidSignUp.catch(function(error) {
          console.error(error);
        });
    }
  } else {
    videoSignUp.pause();
  }
	// All the taxing stuff you do
  if (IsInViewport(videoGenerate)) {
    var promiseVidGenerate = videoGenerate.play();
    if (promiseVidGenerate) {
      promiseVidGenerate.catch(function(error) {
        console.error(error);
      });
    }
  } else {
    videoGenerate.pause();
  }
	// All the taxing stuff you do
  if (IsInViewport(videoFill)) {
    var promiseVidFill = videoFill.play();
    if (promiseVidFill) {
      promiseVidFill.catch(function(error) {
        console.error(error);
      });
    }
  } else {
    videoFill.pause();
  }
}, 250);

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

window.addEventListener('scroll', areVideosInView);
