// Toggle the mobile navigation
function toggleNav() {
  var nav = document.querySelector("#mobileNav");
  var icon = document.querySelector("#icon");
  nav.classList.toggle("hero__mobileNav--hidden");
  icon.classList.toggle("open");
}

window.addEventListener("load", function() {
  var iconWrapper = document.querySelector("#iconWrapper");
  iconWrapper.addEventListener("click", toggleNav);
});
