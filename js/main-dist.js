"use strict";

//@prepros-append modal.js
//@prepros-append index.js

window.addEventListener("load", function () {
  var icon = document.querySelector("#icon");
  icon.addEventListener("click", openMenu);
});

function openMenu() {
  var nav = document.querySelector("#nav");
  var icon = document.querySelector("#icon");
  nav.classList.toggle("nav--open");
  icon.classList.toggle("navlist__icon--open");
  var container = document.querySelector('.container');
  if (nav.classList.contains('nav--open')) {
    if (!container.classList.contains('noscroll')) {
      container.classList.add('noscroll');
    }
  } else {
    if (container.classList.contains('noscroll')) {
      container.classList.remove('noscroll');
    }
  }
}

var modal = {

  /**
   *
   *  @param   id    *Optional*    String     id given to the created modal
   *
   *  @return                      Boolean    true if successful false otherwise
   *
   */
  create: function create() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    var newModal = document.createElement("div");
    newModal = document.appendChild(newModal);
    return true;
  },

  /**
   *
   *  @param   id    *Optional*     String     id of the target modal
   *
   *  @return                       Boolean    true if successful false otherwise
   *
   */
  show: function show() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    if (id === null) {
      return false;
    } else {
      // Get the modal
      var m = document.querySelector("#" + id);
      // Make sure we have a result
      if (m !== null) {
        // Check the classList for modal--hide and remove it
        if (m.classList.contains('modal--hide')) {
          // Remove the hide class
          m.classList.remove('modal--hide');
          return true;
        } else {
          // Already showing
          return true;
        }
      } else {
        // Doesnt exist
        return false;
      }
      return m;
    }
  }

  /**
   *  Check if an element is in the visible viewport
   *
   *  @param      el       HTMLElement
   *
   *  @return              Boolean
   *
   */
};var IsInViewport = function IsInViewport(el) {
  var rect = el.getBoundingClientRect();
  return rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
};

var videoSignUp = document.querySelector("#vidSignUp");
var videoGenerate = document.querySelector("#vidGenerate");
var videoFill = document.querySelector("#vidFill");

var areVideosInView = debounce(function () {
  // All the taxing stuff you do
  if (IsInViewport(videoSignUp)) {
    var promiseVidSignUp = videoSignUp.play();
    if (promiseVidSignUp) {
      //Older browsers may not return a promise, according to the MDN website
      promiseVidSignUp.catch(function (error) {
        console.error(error);
      });
    }
  } else {
    videoSignUp.pause();
  }
  // All the taxing stuff you do
  if (IsInViewport(videoGenerate)) {
    var promiseVidGenerate = videoGenerate.play();
    if (promiseVidGenerate) {
      promiseVidGenerate.catch(function (error) {
        console.error(error);
      });
    }
  } else {
    videoGenerate.pause();
  }
  // All the taxing stuff you do
  if (IsInViewport(videoFill)) {
    var promiseVidFill = videoFill.play();
    if (promiseVidFill) {
      promiseVidFill.catch(function (error) {
        console.error(error);
      });
    }
  } else {
    videoFill.pause();
  }
}, 250);

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;
    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

window.addEventListener('scroll', areVideosInView);