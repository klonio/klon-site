//@prepros-append modal.js
//@prepros-append index.js

window.addEventListener("load", function() {
  var icon = document.querySelector("#icon");
  icon.addEventListener("click", openMenu);
});

function openMenu() {
  var nav = document.querySelector("#nav");
  var icon = document.querySelector("#icon");
  nav.classList.toggle("nav--open");
  icon.classList.toggle("navlist__icon--open");
  var container = document.querySelector('.container');
  if (nav.classList.contains('nav--open')) {
    if (!container.classList.contains('noscroll')) {
      container.classList.add('noscroll');
    }
  } else {
    if (container.classList.contains('noscroll')) {
      container.classList.remove('noscroll');
    }
  }
}
