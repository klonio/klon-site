<section class="price">
  <h2 class="price__heading">Pricing</h2>
  <p class="price__text">
    There are three different plans to choose from when using Klon Privacy Extension.<br> Below you can see the different options and the features that come with each of them.
  </p>
  <div class="price__table">
    <!-- -->
    <div class="price__group price__group--basic">
      <div class="price__title">
        Basic
      </div>
      <div class="price__illustration">
        <img alt="Basic Plan" src="./img/pricing/basic.svg" height="120" width="120">
      </div>
      <div class="price__price">$2.99/mo</div>
      <div class="price__featureTitle">Features</div>
      <div class="price__features">
        <ul class="price__featureList">
          <li class="price__featureItem">1 Identity Per Site</li>
          <li class="price__featureItem">Receive Email</li>
        </ul>
      </div>
      <button class="price__button" id="btnBasic">
        Buy Now
      </button>
    </div>
    <!-- -->
    <div class="price__group price__group--regular">
      <div class="price__title">
        Regular
      </div>
      <div class="price__illustration">
        <img alt="Regular Plan" src="./img/pricing/regular.svg" height="120" width="120">
      </div>
      <div class="price__price">$4.99/mo</div>
      <div class="price__featureTitle">Features</div>
      <div class="price__features">
        <ul class="price__featureList">
          <li class="price__featureItem">5 Identities Per Site</li>
          <li class="price__featureItem">Receive Email</li>
          <li class="price__featureItem">Forward Email To Self</li>
        </ul>
      </div>
      <button class="price__button" id="btnRegular">
        Buy Now
      </button>
    </div>
    <!-- -->
    <div class="price__group price__group--premium">
      <div class="price__title">
        Premium
      </div>
      <div class="price__illustration">
        <img alt="Premium Plan" src="./img/pricing/premium.svg" height="120" width="120">
      </div>
      <div class="price__price">$8.99/mo</div>
      <div class="price__featureTitle">Features</div>
      <div class="price__features">
        <ul class="price__featureList">
          <li class="price__featureItem">25 Identities Per Site</li>
          <li class="price__featureItem">Receive Email</li>
          <li class="price__featureItem">Forward Email To Self</li>
          <li class="price__featureItem">Reply To Email</li>
        </ul>
      </div>
      <button class="price__button" id="btnPremium">
        Buy Now
      </button>
    </div>
    <!-- -->
  </div>
</section>
