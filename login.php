<!-- Body of the document -->
<?php
  session_start();
 ?>
<section id="login">
  <h2 class="login__title">Login</h2>
  <div class="login__block">
    <form class="login__form" name="login">
      <img alt="Klon Login Illustration" src="./img/login/login-illustration.svg" height="300" width="100%">
      <input class="login__email" type="email" placeholder="Email" name="email" required>
      <input class="login__password" type="password" placeholder="Password" name="password" required>
      <button class="login__button">Login</button>
    </form>
  </div>
</section>
