<?php
  include_once("arrays.php");
  $uri = $_SERVER['REQUEST_URI'];
  $countSlashes = substr_count($uri,"/");
  // echo "COUNT: " . $countSlashes;
  // echo "URI: " . $uri;
  $indexOfSlash = strrpos($uri, "/");
  // echo "<br>Index: " . $indexOfSlash;
  $request = substr($uri, $indexOfSlash, strlen($uri));
  $b = substr($request, 1,strlen($request));
  $fourohfourd = true;
  // echo "<br>SLASH: " . $b;
  foreach ($pages as $navItem) {
    //
    if(in_array(strtolower($b), $navItem)) {
      include_once("header.php");
      include_once($navItem['slug']);
      include_once("footer.php");
      $fourohfourd = false;
    }
  }
  // If the page is not found then send them to the 404
  if ($fourohfourd === true) {
    include_once('main.php');
  }
?>
