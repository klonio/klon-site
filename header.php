<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,shrink-to-fit=no"name=viewport>
    <meta property="fb:app_id" content="368975784033177"/>
    <meta property="og:title" content="Klon Privacy"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://klon.io"/>
    <meta property="og:image" content="https://klon.io/img/openGraphImage.png"/>
    <meta property="og:image:type" content="image/png"/>
    <meta property="og:image:height" content="1200"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:alt" content="Isometric design illustrating various aspects of Klon Privacy Extension."/>
    <meta property="og:site_name" content="Klon Privacy"/>
    <meta property="og:description" content="Klon is a privacy company. We help protect your identity and make logging in to your favorites websites a breeze with our easy to use browser extension."/>
    <title>Klon</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
   <link href="css/main.css" type="text/css" rel="stylesheet">
  </head>
  <body>
    <div class="container container--white">
      <!-- This is the header strip across the top of the page -->
      <header class="header">
        <h1 id="theTop">
        <!-- Small logo in the top left corner -->
          <a class="header__logoAnchor" href="./"><img class="header__logo" src="./img/logo.svg" height="" width="" alt="Klon"></a>
          <span class="hidden" aria-hidden="true">Klon</span>
        </h1>
        <div class="navlist__iconWrapper">
          <svg class="navlist__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" id="icon">
            <path id="line1" d="M0,2L28,2L28,5L0,5z"/>
            <path id="line2" d="M0,11L28,11L28,14L0,14z"/>
            <path id="line3" d="M0,20L28,20L28,23L0,23z"/>
          </svg>
        </div>
        <nav class="nav" id="nav">
          <!-- Navigation spans the rest of the top stripe -->
          <ul class="navlist">
            <?php
              foreach ($mainNav as $nav) {
                print("<li class=\"navlist__item\"><a class=\"navlist__anchor\" href=\"" . $nav['search'] . "\">".$nav['title']."</a></li>");
              }
            ?>
          </ul>
        </nav>
      </header>
