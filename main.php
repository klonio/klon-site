<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" lang="en">
    <meta content="width=device-width,initial-scale=1,shrink-to-fit=no"name=viewport>
    <meta property="fb:app_id" content="368975784033177"/>
    <meta property="og:title" content="Klon Privacy"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://klon.io"/>
    <meta property="og:image" content="https://klon.io/img/openGraphImage.png"/>
    <meta property="og:image:type" content="image/png"/>
    <meta property="og:image:height" content="1200"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:alt" content="Isometric design illustrating various aspects of Klon Privacy Extension."/>
    <meta property="og:site_name" content="Klon Privacy"/>
    <meta property="og:description" content="Klon is a privacy company. We help protect your identity and make logging in to your favorites websites a breeze with our easy to use browser extension."/>
    <title>Klon</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body>
    <div id="theTop"></div>
    <div class="container">
      <div class="hero">
        <!-- Mobile and Tablet hero -->
        <div class="hero__mobile">
          <div id="iconWrapper" class="mobileNav__iconWrapper">
            Menu
            <svg class="mobileNav__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" id="icon">
              <path id="line1" d="M0,2L28,2L28,5L0,5z"/>
              <path id="line2" d="M0,11L28,11L28,14L0,14z"/>
              <path id="line3" d="M0,20L28,20L28,23L0,23z"/>
            </svg>
          </div>
            <nav id="mobileNav" class="hero__mobileNav hero__mobileNav--hidden">
              <div class="mobileNav__wrapper">
                <ul class="mobileNav__list">
                  <li class="mobileNav__listItem">
                    <a href="./download">Download</a>
                  </li>
                  <li class="mobileNav__listItem">
                    <a href="./about">About</a>
                  </li>
                  <li class="mobileNav__listItem">
                    <a href="./tech">Technical</a>
                  </li>
                  <li class="mobileNav__listItem">
                    <a href="./faq">FAQ</a>
                  </li>
                </ul>
              </div>
            </nav>
          <img class="hero__logo" alt="Klon Logo" height="52" width="254" src="img/logo.svg"/>
          <img class="hero__image" alt="Hero Image" src="img/hero.svg"/>
          <!-- <h2 class="hero__tagline">Your privacy extension</h2> -->
          <div class="hero__mobileButtonWrapper">
            <a class="hero__btnSignUp" href="./download" id="btnSignUp">Download Now</a>
            <button class="hero__btnLearnMore" id="btnLearnMore">Learn More</button>
          </div>
        </div>
        <!-- Laptop Up Hero Below this line -->
        <svg id="hero" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1200.1 1205.3">
          <path d="M0 10.3h1200v1195H0V10.3z" class="st0"/>
          <g id="XMLID_22_">
            <path id="HeroShadow_2_" fill="#255bde" d="M405.1 11.3S520 129.4 427.3 253.8C398.1 293 283.4 357.5 310.5 477.4c25.8 114.3 74.2 212.7-73.9 230.2C98.1 708.9 0 823 0 961.9c0 139.7 101.9 243 241.6 243s250.3-104.8 250.3-244.5c0-52.8-16.2-101.9-43.9-142.5-6.8-10-14.3-19.4-22.4-28.3-14.9-22.9-28.3-54.9-7.2-77.8 24-26 113.9 68.4 283.2 22.7 152.5-41.2 276.2 25.7 322 88.7 53.1 73 176.5 75 176.5 75V10.3h-795v1z"/>
            <g id="HeroBG_2_">
              <circle id="XMLID_32_" cx="258" cy="949.4" r="252.9" class="st2"/>
              <linearGradient id="heroMainBackground_2_" x1="722.45" x2="722.45" y1="468.256" y2="1148.387" gradientTransform="matrix(1 0 0 -1 0 1216.596)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".285" stop-color="#061765"/>
                <stop offset=".99" stop-color="#143da1"/>
              </linearGradient>
              <path id="heroMainBackground_1_" fill="url(#heroMainBackground_2_)" d="M467 806.9s-66-66.6-29.7-106.1c24-26 113.9 68.4 283.2 22.7 152.5-41.1 276.8 25.8 323.5 88.8 53.1 73 156 75 156 75v-877H424.1s114.9 107.6 22.2 232c-29.2 39.2-143.9 104-116.8 223.9 26.4 117 76.5 217.4-84.6 231.4"/>
            </g>
            <g id="nav">
              <a class="nav__item" xlink:href="./tech">
                <text transform="translate(1068 50.256)">
                  Technical
                </text>
                <path class="nav__underline" d="M1068 54.3h98v2h-98v-2z" class="st0"/>
              </a>
              <a class="nav__item" xlink:href="./roadmap">
                <text transform="translate(923.887 50.256)">
                  Roadmap
                </text>
                <path class="nav__underline" d="M923.9 54.3h84.1v2h-84.1v-2z" class="st0"/>
              </a>
              <a class="nav__item" xlink:href="./download">
                <text transform="translate(760 50.256)">
                  Download
                </text>
                <path class="nav__underline" d="M760 54.3H864v2H760v-2z" class="st0"/>
              </a>
              <a class="nav__item" xlink:href="./about">
                <text transform="translate(639.415 50.256)">
                  About
                </text>
                <path class="nav__underline" d="M639.4 54.3H700v2h-60.6v-2z" class="st0"/>
              </a>
              <a class="nav__item" xlink:href="./faq">
                <text transform="translate(519.415 50.256)">
                  FAQ
                </text>
                <path class="nav__underline" d="M519.4 54.3H556v2h-36.6v-2z" class="st0"/>
              </a>
            </g>
            <g id="logo">
              <g id="XMLID_27_">
                <path id="XMLID_31_" d="M200.9 114.8l-15.3 15.9 16.6 16.9v.3h-6.7l-14.8-15.2v15.2h-5.3v-33.3h5.3V129l13.5-14.4h6.6l.1.2z" class="st6"/>
                <path id="XMLID_30_" d="M230.5 114.6v28.3H248v4.9h-22.9v-33.3h5.4v.1z" class="st6"/>
                <path id="XMLID_29_" d="M303.3 131.4c0 9-5.5 17.2-17 17.2-11.6 0-17-8.4-17-17.2 0-10.1 7.1-17.4 17.1-17.4 11.3.1 16.9 8.4 16.9 17.4zm-17 12.3c8.3 0 11.7-5.8 11.7-12.2 0-6.1-3.5-12.5-11.6-12.6-7.7 0-11.8 5.4-11.8 12.6 0 5.9 3.4 12.2 11.7 12.2z" class="st6"/>
                <path id="XMLID_28_" d="M351.6 138.3v-23.7h5.3v33.3h-4L334 124.4v23.5h-5.4v-33.3h4.1l18.9 23.7z" class="st6"/>
              </g>
              <g id="XMLID_23_">
                <linearGradient id="XMLID_3_" x1="34.732" x2="130.231" y1="1023.823" y2="1126.029" gradientTransform="matrix(1 0 0 -1 0 1216.596)" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#00074d"/>
                  <stop offset="1" stop-color="#143da1"/>
                </linearGradient>
                <circle id="XMLID_26_" cx="110.3" cy="111.9" r="7" fill="url(#XMLID_3_)"/>
                <linearGradient id="XMLID_7_" x1="52.741" x2="148.24" y1="1006.997" y2="1109.202" gradientTransform="matrix(1 0 0 -1 0 1216.596)" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#00074d"/>
                  <stop offset="1" stop-color="#143da1"/>
                </linearGradient>
                <circle id="XMLID_25_" cx="110.3" cy="148" r="7" fill="url(#XMLID_7_)"/>
                <linearGradient id="XMLID_10_" x1="60.113" x2="155.612" y1="1000.108" y2="1102.313" gradientTransform="matrix(1 0 0 -1 0 1216.596)" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#00074d"/>
                  <stop offset="1" stop-color="#143da1"/>
                </linearGradient>
                <path id="XMLID_24_" fill="url(#XMLID_10_)" d="M142.5 140.3c-1.8-.2-3.4-.8-4.9-1.6-2.9-1.8-4.9-5.1-4.9-8.8 0-4.1 2.4-7.7 5.9-9.3 1.2-.5 2.4-.9 3.7-1 1.7-.1 3.3-.9 4.6-2.1 3-3 2.9-8-.4-10.9-2.8-2.4-7-2.4-9.8 0-1.4 1.3-2.2 2.9-2.4 4.7 0 .4-.1.7-.2 1.1-.2 5.5-4.6 9.9-10 10-.4.1-.7.1-1.1.2h-.2c-.2 0-.3.1-.5.1-.3.1-.5.1-.8.2-.2.1-.4.1-.5.2l-.6.3c-.2.1-.4.2-.6.4-.2.1-.4.3-.6.4-.1 0-.1.1-.2.1l-.3.3-.4.4c-.1.1-.1.2-.2.3-.1.2-.2.3-.4.5s-.3.5-.4.7l-.3.6c-.1.2-.2.5-.2.7-.1.2-.1.4-.2.7 0 .2-.1.5-.1.7 0 .2-.1.5-.1.7v.7c0 .3.1.6.1.9.1.4.2.7.3 1 .1.2.1.3.2.5.1.3.3.6.4.8.1.2.2.3.3.4.1.2.2.4.4.5l.2.2.2.2c.2.2.4.3.5.4s.2.2.4.3c.3.2.7.5 1.1.6.1 0 .1.1.2.1.4.2.9.3 1.3.5.1 0 .3 0 .4.1.2 0 .5.1.7.1.8.1 1.6.2 2.4.5 4.6.8 8.1 4.7 8.5 9.4.1.4.1.8.2 1.2.1 1.7.9 3.3 2.1 4.6 3 3 8 2.9 10.9-.4 2.4-2.8 2.4-7-.1-9.8-1.3-1.4-2.9-2.2-4.6-2.4z"/>
              </g>
            </g>
            <a xlink:href="./download" class="hero__anchor">
              <g id="btnSignUp" class="hero__button hero__button--signup">
                <path class="hero__btnSignup" d="M387.9 926.7H141.4c-23.5 0-42.7-19.2-42.7-42.7v-5.5c0-23.5 19.2-42.7 42.7-42.7H388c23.5 0 42.7 19.2 42.7 42.7v5.5c0 23.5-19.3 42.7-42.8 42.7z"/>
                <text class="hero__btnText hero__btnText--signUp" href="./download">
                  Download
                </text>
                <path fill="#fff" d="M386 888.3l12-7-12-7v14z" class="hero__buttonArrow hero__buttonArrow--right"/>
              </g>
            </a>
            <text class="hero__btnText hero__btnText--secondary" transform="translate(230 969)">
              -or-
            </text>
            <a class="hero__anchor" id="aLearnMore">
              <g id="btnLearnMore" class="hero__button hero__button--learnMore">
                <path class="hero__btnLearnMore" d="M354.7 1081.9H172.2c-23.5 0-42.7-19.2-42.7-42.7s19.2-42.7 42.7-42.7h182.5c23.5 0 42.7 19.2 42.7 42.7.1 23.5-19.2 42.7-42.7 42.7z"/>
                <text class="hero__btnText hero__btnText--learn" transform="translate(0 10.298)">
                  Learn More
                </text>
                <path class="hero__buttonArrow hero__buttonArrow--down" d="M256 1055.3l7 12 7-12h-14z"/>
              </g>
            </a>
          </g>
          <path d="M897.1 479.2l162.7-93.1 21.4-12.3 52.4-30.3-236.5-135.7-46.9 27.1-43.3-24.9" class="st10"/>
          <path d="M850.6 289l-94.5 54.5 47 27.1-47 27.2 141 81.4" class="st10"/>
          <path fill="none" stroke="#71cbd5" stroke-linecap="square" stroke-miterlimit="10" stroke-width="1.276" d="M527.3 487.6l134.8 78 192.2-111"/>
          <path d="M405.5 464l134.2-78.6" class="st12"/>
          <path fill="none" stroke="#71cbd5" stroke-dasharray="0 8.933" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2.552" d="M617.2 588.9l44.9 25.9 12.4-7.1M891 482.6l-41.7 23.6"/>
          <path d="M607.5 587.1c-1.8-1-1.8-2.7 0-3.8s4.8-1 6.6 0 1.8 2.7 0 3.8-4.8 1-6.6 0zm1.5-.9c1 .6 2.5.6 3.5 0s1-1.5 0-2-2.5-.6-3.5 0-.9 1.4 0 2zM538.8 385.8c-1.8-1-1.8-2.7 0-3.8s4.8-1 6.6 0c1.8 1 1.8 2.7 0 3.8s-4.8 1.1-6.6 0zm1.5-.8c1 .6 2.5.6 3.5 0s1-1.5 0-2c-1-.6-2.5-.6-3.5 0-1 .5-1 1.4 0 2z" class="st14"/>
          <path d="M486.9 511.2L405.5 464m121.8 23.6l-40.4 23.6" class="st12"/>
          <path id="bigStackShadow" d="M1132.1 343.5L944.1 452l-188-108.5 188-108.5z" class="st15"/>
          <path fill="#143da1" d="M802.3 343.3L944 260.2l141.1 83.3-140.5 81.4z" opacity=".7"/>
          <g id="bigStackShadow" class="st17">
            <path d="M1045.7 360.3l-102.5 59.2-99.1-58.1 101.8-58.5z" class="st2 shadow1"/>
          </g>
          <g id="dominoShadow1" class="st20">
            <path d="M788.8 265L571.6 390.3l-46.4-27.5 216.3-124.3z" class="st2 shadow2"/>
          </g>
          <path id="laptopShadow3" d="M382.1 509.9l198.4 116.3 158.7-91.7L540.8 420z" class="st15"/>
          <path id="laptopShadow2" fill="#143da1" d="M698.9 534.5l-158.2-91.3-118.2 67 158.1 92.7z" opacity=".2"/>
          <path id="laptopShadow1" d="M686.3 536.5l-104.5 60.6-150.1-88 103.7-59.8z" class="st2 shadow2"/>
          <path fill="none" d="M431.4 512.7l133.8-77.2m-142.9 84.7l133.8-77.3M420.5 532l133.8-77.2m-129.4 76.8l133.8-77.2"/>
          <g id="emailStackShadow_1_" class="st17">
            <path d="M794.1 367.9l-57.9 33.5-56-32.9 57.5-33z" class="st2 shadow3"/>
          </g>
          <path d="M836.1 388.1h.1" class="st14"/>
          <g id="profileSlabShadow_1_" class="st29">
            <path d="M732.6 596.6l-37.9 21.9L658 597l37.7-21.7z" class="st2 shadow3"/>
          </g>
          <g id="locationSlabShadow_1_" class="st32">
            <path d="M772.7 571.8l-38 21.9-36.7-21.5 37.7-21.6z" class="st2 shadow2"/>
          </g>
          <path id="moneySlabShadow" d="M814.5 548.4l-37.9 21.9-36.7-21.5 37.7-21.7z" class="st2 shadow3"/>
          <g id="mailSlabShadow" class="st29">
            <path d="M854.3 524.9l-38 21.9-36.7-21.5 37.7-21.7z" class="st2 shadow2"/>
          </g>
          <path id="serverShadow2" d="M751.8 465.1c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0 9.6-5.5 9.6-14.5 0-20.1-9.6-5.6-25.2-5.6-34.8 0z" class="st2 shadow4"/>
          <path id="spamGuardShadow" d="M879.5 482.5c-17 9.8-17 25.7 0 35.6s44.6 9.8 61.6 0 17-25.7 0-35.6c-17-9.9-44.6-9.9-61.6 0z" class="st2 shadow5" opacity=".4"/>
          <path id="rejectedEmailShadow" d="M1084.3 467.3l-.2-.5-.3-1-3.6-10.5-1.6.2-34.2 4.1-1.4.2v1.6l.3.9 3.7 10.5.3.9 3.5-.5 28.7-3.4 4.9-.6v-1.6z" class="st2 shadow5" opacity=".2"/>
          <g id="thirdEmailShadow" class="st47">
            <path fill="#000330" d="M1090.1 706l53.8 31.2v3.4l-30.4 17.5-53.7-31.2v-3.4z" class="shadow5"/>
          </g>
          <g id="secondEmailShadow" class="st47">
            <path d="M1058.8 544.6l34 19.7v2.1l-19.2 11.1-34-19.7v-2.2z" class="st2 shadow6"/>
          </g>
          <g id="firstEmailShadow" class="st47">
            <path d="M980.6 508.6l30.1 17.5v1.9l-17 9.8-30.1-17.5v-1.9z" class="st2 shadow6"/>
          </g>
          <g id="validEmailShadow" class="st47">
            <path d="M812.8 401.7l30.1 17.5v1.8l-17.1 9.9-30-17.5v-1.9z" class="st2 shadow2"/>
          </g>
          <path id="serverShadow1" d="M1059.3 340.1c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0s9.6-14.5 0-20.1c-9.6-5.5-25.1-5.5-34.8 0z" class="st2 shadow3"/>
          <g id="shieldShadow" class="st20">
            <path d="M758.6 224.7c-14.5 8.4-14.5 21.9 0 30.3 14.5 8.4 38 8.4 52.5 0s14.5-21.9 0-30.3-38-8.4-52.5 0z" class="st2 shadow1"/>
          </g>
          <g id="shadow1" class="st17">
            <path d="M505.5 436.6L447.6 470l-56.1-32.8 57.6-33.1z" class="st2 shadow3"/>
          </g>
          <g id="Slab1">
            <linearGradient id="SVGID_19_" x1="687.619" x2="786.668" y1="371.161" y2="371.161" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_19_)" d="M784.5 365.8L742 390.4c-2.7 1.5-6.7 1.7-9 .4l-43.8-25.3c-1.1-.6-1.6-1.5-1.5-2.4v-12.3h99v12c.1.9-.7 2.1-2.2 3z"/>
            <linearGradient id="SVGID_20_" x1="665.298" x2="764.347" y1="350.689" y2="350.689" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset=".972" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_20_)" d="M733 378.4l-43.8-25.3c-2.3-1.3-2-3.7.6-5.2l42.5-24.5c2.7-1.5 6.7-1.7 9-.4l43.8 25.3c2.3 1.3 2 3.7-.6 5.2L742 378c-2.7 1.5-6.7 1.7-9 .4z"/>
            <linearGradient id="SVGID_21_" x1="685.97" x2="759.404" y1="362.039" y2="345.751" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_21_)" d="M737.4 322.6c1.4 0 2.7.3 3.7.8l43.8 25.3c.9.5 1.3 1.1 1.3 1.8 0 .9-.7 1.8-2 2.5l-42.5 24.5c-1.3.8-3.1 1.2-4.9 1.2-1.4 0-2.7-.3-3.7-.8l-43.8-25.3c-.9-.5-1.3-1.1-1.3-1.8 0-.9.7-1.8 2-2.5l42.5-24.5c1.4-.8 3.2-1.2 4.9-1.2m0-.5c-1.8 0-3.6.4-5.1 1.3l-42.5 24.5c-2.7 1.5-3 3.9-.6 5.2l43.8 25.3c1 .6 2.4.9 3.9.9 1.8 0 3.6-.4 5.1-1.3l42.5-24.5c2.7-1.5 3-3.9.6-5.2L741.3 323c-1-.6-2.4-.9-3.9-.9z"/>
            <g>
              <path d="M700.9 369.1s0 .2-.1.2c0 0-.1 0-.2-.1l-8.7-5c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l8.7 5c.3.3.6.8.6 1.1zM705.7 368.7s0 .2-.1.2c0 0-.1 0-.2-.1l-13.5-7.8c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l13.5 7.8c.3.2.6.7.6 1.1zM702.4 363.7s0 .2-.1.2c0 0-.1 0-.2-.1l-10.3-5.9c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l10.3 5.9c.3.2.6.7.6 1.1z" class="st14"/>
            </g>
          </g>
          <g id="Slab_2_3_">
            <linearGradient id="SVGID_22_" x1="687.619" x2="786.668" y1="355.803" y2="355.803" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_22_)" d="M784.5 350.5L742 375c-2.7 1.5-6.7 1.7-9 .4l-43.8-25.3c-1.1-.6-1.6-1.5-1.5-2.4v-12.3h99v12c.1 1-.7 2.2-2.2 3.1z"/>
            <linearGradient id="SVGID_23_" x1="664.929" x2="763.978" y1="335.33" y2="335.33" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_23_)" d="M733 363l-43.8-25.3c-2.3-1.3-2-3.7.6-5.2l42.5-24.5c2.7-1.5 6.7-1.7 9-.4l43.8 25.3c2.3 1.3 2 3.7-.6 5.2L742 362.7c-2.7 1.5-6.7 1.7-9 .3z"/>
            <linearGradient id="SVGID_24_" x1="685.97" x2="759.404" y1="346.681" y2="330.392" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_24_)" d="M737.4 307.2c1.4 0 2.7.3 3.7.8l43.8 25.3c.9.5 1.3 1.1 1.3 1.8 0 .9-.7 1.8-2 2.5l-42.5 24.5c-1.3.8-3.1 1.2-4.9 1.2-1.4 0-2.7-.3-3.7-.8l-43.8-25.3c-.9-.5-1.3-1.1-1.3-1.8 0-.9.7-1.8 2-2.5l42.5-24.5c1.4-.8 3.2-1.2 4.9-1.2m0-.5c-1.8 0-3.6.4-5.1 1.3l-42.5 24.5c-2.7 1.5-3 3.9-.6 5.2L733 363c1 .6 2.4.9 3.9.9 1.8 0 3.6-.4 5.1-1.3l42.5-24.5c2.7-1.5 3-3.9.6-5.2l-43.8-25.3c-1-.6-2.4-.9-3.9-.9z"/>
            <g>
              <path d="M703.7 349.1s0 .2-.1.2c0 0-.1 0-.2-.1l-11.5-6.7c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l11.5 6.7c.3.2.6.7.6 1.1zM701 350.7s0 .2-.1.2c0 0-.1 0-.2-.1l-8.8-5.1c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l8.8 5.1c.3.2.6.7.6 1.1zM705.7 356.6s0 .2-.1.2c0 0-.1 0-.2-.1l-13.5-7.8c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l13.5 7.8c.3.2.6.7.6 1.1z" class="st14"/>
            </g>
          </g>
          <g id="Slab_3_3_">
            <linearGradient id="SVGID_25_" x1="687.619" x2="786.668" y1="340.444" y2="340.444" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_25_)" d="M784.5 335.1L742 359.6c-2.7 1.5-6.7 1.7-9 .4l-43.8-25.3c-1.1-.6-1.6-1.5-1.5-2.4V320h99v12c.1 1-.7 2.2-2.2 3.1z"/>
            <linearGradient id="SVGID_26_" x1="782.738" x2="710.555" y1="314.905" y2="322.926" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_26_)" d="M733 347.7l-43.8-25.3c-2.3-1.3-2-3.7.6-5.2l42.5-24.5c2.7-1.5 6.7-1.7 9-.4l43.8 25.3c2.3 1.3 2 3.7-.6 5.2L742 347.3c-2.7 1.5-6.7 1.7-9 .4z"/>
            <linearGradient id="SVGID_27_" x1="685.97" x2="759.404" y1="331.322" y2="315.034" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_27_)" d="M737.4 291.8c1.4 0 2.7.3 3.7.8l43.8 25.3c.9.5 1.3 1.1 1.3 1.8 0 .9-.7 1.8-2 2.5l-42.5 24.5c-1.3.8-3.1 1.2-4.9 1.2-1.4 0-2.7-.3-3.7-.8L689.4 322c-.9-.5-1.3-1.1-1.3-1.8 0-.9.7-1.8 2-2.5l42.5-24.5c1.3-.9 3.1-1.4 4.8-1.4m0-.4c-1.8 0-3.6.4-5.1 1.3l-42.5 24.5c-2.7 1.5-3 3.9-.6 5.2l43.8 25.3c1 .6 2.4.9 3.9.9 1.8 0 3.6-.4 5.1-1.3l42.5-24.5c2.7-1.5 3-3.9.6-5.2l-43.8-25.3c-1-.6-2.4-.9-3.9-.9z"/>
            <g>
              <path d="M726.7 326c-5.8-3.3-5.8-8.7 0-12.1s15.1-3.3 20.9 0c5.8 3.3 5.8 8.7 0 12.1s-15.1 3.3-20.9 0zm.9-.5c5.3 3 13.8 3 19.1 0 5.3-3 5.3-8 0-11s-13.8-3-19.1 0c-5.2 3-5.2 7.9 0 11z" class="st14"/>
            </g>
            <g>
              <path d="M705.7 335.2s0 .2-.1.2c0 0-.1 0-.2-.1l-13.5-7.8c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l13.5 7.8c.3.3.6.8.6 1.1zM700.9 335.7s0 .2-.1.2c0 0-.1 0-.2-.1l-8.7-5c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l8.7 5c.3.2.6.7.6 1.1zM698.1 337.3s0 .2-.1.2c0 0-.1 0-.2-.1l-6-3.4c-.3-.2-.6-.7-.6-1.1 0 0 0-.2.1-.2 0 0 .1 0 .2.1l6 3.4c.3.2.6.7.6 1.1z" class="st14"/>
            </g>
          </g>
          <g id="Server_1_1_">
            <linearGradient id="SVGID_28_" x1="903.632" x2="885.387" y1="341.718" y2="373.32" gradientUnits="userSpaceOnUse">
              <stop offset=".006" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_28_)" d="M850.2 308.4v34.4l94.1 54.3v-34.4l-74.2-42.9-9.9-5.7z"/>
            <linearGradient id="SVGID_29_" x1="705.553" x2="1028.33" y1="308.359" y2="308.359" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_29_)" d="M850.2 308.4l10 5.7 9.9 5.7 74.2 42.9 74.2-42.9 10-5.7 9.9-5.7-94.1-54.4z"/>
            <linearGradient id="SVGID_30_" x1="892.713" x2="986.789" y1="352.731" y2="352.731" gradientUnits="userSpaceOnUse">
              <stop offset=".006" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_30_)" d="M944.3 362.7v34.4l94.1-54.3v-34.4l-9.9 5.7-10 5.7z"/>
            <g>
              <path d="M920.2 376.8v-21.9l19 11v21.9z" class="st0"/>
              <path d="M921.5 357.2l16.4 9.5v19l-16.4-9.5v-19m-2.5-4.5v24.9l21.5 12.4v-24.9L919 352.7z" class="st75"/>
            </g>
            <path d="M854 323.7v6.7l61.6 35.6v-6.8zm29.1 8.3v5.6l32.5 18.8v-5.6zm-29.1 1.3v6.8l61.6 35.5v-6.7z" class="st75"/>
            <g>
              <path d="M872.8 329.4c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.6.3 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.6.3 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 .1 1-.6zm-2.5-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.5.3 1 .1 1-.6zm-2.6-1.4c0-.6-.5-1.4-1-1.7s-1-.1-1 .6c0 .6.5 1.4 1 1.7.5.3 1 0 1-.6zM880.5 333.9c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.6.3 1 0 1-.6zm-2.5-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.5.3 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 0 1-.6z" class="st14"/>
            </g>
          </g>
          <g id="Server_2_1_">
            <path d="M850.2 269.3v34.4l94.1 54.3v-34.4l-74.2-42.9-9.9-5.7z" class="st76"/>
            <linearGradient id="SVGID_31_" x1="916.065" x2="979.611" y1="318.181" y2="208.116" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_31_)" d="M850.2 269.3l10 5.7 9.9 5.7 74.2 42.9 74.2-42.9 10-5.7 9.9-5.7-94.1-54.4z"/>
            <linearGradient id="SVGID_32_" x1="772.556" x2="960.717" y1="269.256" y2="269.256" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_32_)" d="M850.2 269.3l10 5.7 9.9 5.7 74.2 42.9 74.2-42.9 10-5.7 9.9-5.7-94.1-54.4z"/>
            <g>
              <linearGradient id="SVGID_33_" x1="816.074" x2="1009.824" y1="313.628" y2="313.628" gradientUnits="userSpaceOnUse">
                <stop offset=".006" stop-color="#143da1"/>
                <stop offset="1" stop-color="#00074d"/>
              </linearGradient>
              <path fill="url(#SVGID_33_)" d="M944.3 323.6V358l94.1-54.3v-34.4l-9.9 5.7-10 5.7z"/>
            </g>
            <g>
              <path d="M920.2 337.7v-21.9l19 11v21.9z" class="st0"/>
              <path d="M921.5 318l16.4 9.5v19l-16.4-9.5v-19m-2.5-4.4v24.9l21.5 12.4V326L919 313.6z" class="st75"/>
            </g>
            <path d="M854 284.5v6.8l61.6 35.5v-6.7zm29.1 8.4v5.6l32.5 18.8v-5.6zm-29.1 1.3v6.8l61.6 35.5v-6.7z" class="st75"/>
            <g>
              <path d="M872.8 290.3c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.5.3 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.6.3 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.6.3 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 .1 1-.6zm-2.5-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 .1 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.5.3 1 .1 1-.6zm-2.6-1.4c0-.6-.5-1.4-1-1.7s-1-.1-1 .6.5 1.4 1 1.7c.5.3 1 0 1-.6zM880.5 294.8c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.6.3 1 0 1-.6zm-2.5-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.5.3 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 0 1-.6z" class="st14"/>
            </g>
          </g>
          <g id="Top_1_">
            <linearGradient id="SVGID_34_" x1="944.317" x2="1038.398" y1="286.001" y2="286.001" gradientUnits="userSpaceOnUse">
              <stop offset=".006" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_34_)" d="M944.3 307.4l94.1-54.3v11.5l-94.1 54.3z"/>
            <linearGradient id="SVGID_35_" x1="1041.873" x2="877.658" y1="253.104" y2="253.104" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".964" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_35_)" d="M850.2 253.1l94.1-54.3 94.1 54.3-94.1 54.3z"/>
            <linearGradient id="SVGID_36_" x1="842.611" x2="1030.772" y1="253.104" y2="253.104" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_36_)" d="M944.3 199.3l93.1 53.8-93.1 53.8-93.1-53.8 93.1-53.8m0-.5l-94.1 54.3 94.1 54.3 94.1-54.3-94.1-54.3z"/>
            <g>
              <path d="M944.3 307.4l-94.1-54.3v11.5l94.1 54.3z" class="st76"/>
            </g>
          </g>
          <linearGradient id="SVGID_37_" x1="607.194" x2="656.413" y1="467.598" y2="467.598" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00074d"/>
            <stop offset=".03" stop-color="#00074d"/>
            <stop offset=".464" stop-color="#00074d"/>
            <stop offset=".55" stop-color="#143da1"/>
            <stop offset=".99" stop-color="#143da1"/>
          </linearGradient>
          <path fill="url(#SVGID_37_)" d="M744.6 456.2v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10v-8.7h-49.2z"/>
          <path d="M751.8 446.1c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0 9.6-5.5 9.6-14.5 0-20.1-9.6-5.5-25.2-5.5-34.8 0z" class="st76"/>
          <linearGradient id="SVGID_38_" x1="578.307" x2="622.924" y1="456.166" y2="456.166" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset=".01" stop-color="#143da1"/>
            <stop offset=".45" stop-color="#143da1"/>
            <stop offset=".536" stop-color="#00074d"/>
            <stop offset=".97" stop-color="#00074d"/>
            <stop offset="1" stop-color="#1f2243"/>
          </linearGradient>
          <path fill="url(#SVGID_38_)" d="M746.9 456.2c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9s-11.9 1.4-16.2 3.9c-3.9 2.3-6.1 5.1-6.1 8.1z"/>
          <path d="M747.3 458.4c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          <linearGradient id="SVGID_39_" x1="735.618" x2="784.836" y1="456.166" y2="456.166" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_39_)" d="M769.2 442.4c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6 0 3.6-2.5 7-7 9.6-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.6-2.6 10.7-4.1 17.2-4.1m0-.4c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.9-11.1-4.2-17.4-4.2z"/>
          <linearGradient id="SVGID_40_" x1="607.194" x2="656.413" y1="453.984" y2="453.984" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00074d"/>
            <stop offset=".03" stop-color="#00074d"/>
            <stop offset=".464" stop-color="#00074d"/>
            <stop offset=".55" stop-color="#143da1"/>
            <stop offset=".99" stop-color="#143da1"/>
          </linearGradient>
          <path fill="url(#SVGID_40_)" d="M744.6 442.6v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10v-8.7h-49.2z"/>
          <path d="M751.8 432.5c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0 9.6-5.5 9.6-14.5 0-20.1-9.6-5.5-25.2-5.5-34.8 0z" class="st76"/>
          <linearGradient id="SVGID_41_" x1="578.307" x2="622.924" y1="442.553" y2="442.553" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset=".01" stop-color="#143da1"/>
            <stop offset=".45" stop-color="#143da1"/>
            <stop offset=".536" stop-color="#00074d"/>
            <stop offset=".97" stop-color="#00074d"/>
            <stop offset="1" stop-color="#1f2243"/>
          </linearGradient>
          <path fill="url(#SVGID_41_)" d="M746.9 442.6c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9s-11.9 1.4-16.2 3.9c-3.9 2.3-6.1 5.1-6.1 8.1z"/>
          <path d="M747.3 444.7c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          <linearGradient id="SVGID_42_" x1="735.618" x2="784.836" y1="442.553" y2="442.553" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_42_)" d="M769.2 428.8c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6 0 3.6-2.5 7-7 9.6-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.6-2.6 10.7-4.1 17.2-4.1m0-.5c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.8-11.1-4.2-17.4-4.2z"/>
          <linearGradient id="SVGID_43_" x1="607.194" x2="656.413" y1="440.37" y2="440.37" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00074d"/>
            <stop offset=".03" stop-color="#00074d"/>
            <stop offset=".464" stop-color="#00074d"/>
            <stop offset=".55" stop-color="#143da1"/>
            <stop offset=".99" stop-color="#143da1"/>
          </linearGradient>
          <path fill="url(#SVGID_43_)" d="M744.6 428.9v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10v-8.7h-49.2z"/>
          <path d="M751.8 418.9c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0 9.6-5.5 9.6-14.5 0-20.1-9.6-5.6-25.2-5.6-34.8 0z" class="st76"/>
          <linearGradient id="SVGID_44_" x1="605.732" x2="650.35" y1="428.939" y2="428.939" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset=".01" stop-color="#143da1"/>
            <stop offset=".536" stop-color="#00074d"/>
            <stop offset="1" stop-color="#1f2243"/>
          </linearGradient>
          <path fill="url(#SVGID_44_)" d="M746.9 428.9c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9s-11.9 1.4-16.2 3.9c-3.9 2.3-6.1 5.2-6.1 8.1z"/>
          <path d="M747.3 431.1c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9s11.9-1.4 16.3-3.9c2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          <linearGradient id="SVGID_45_" x1="735.618" x2="784.836" y1="428.94" y2="428.94" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_45_)" d="M769.2 415.2c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6 0 3.6-2.5 7-7 9.6-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.6-2.6 10.7-4.1 17.2-4.1m0-.5c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.8-11.1-4.2-17.4-4.2z"/>
          <path d="M793.9 456.1v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10v-2.2c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.8 7.2-6.4 7.2-10z" class="st75"/>
          <path d="M793.9 442.5v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10v-2.2c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.7 7.2-6.3 7.2-10z" class="st75"/>
          <path d="M793.9 428.9v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10v-2.2c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.7 7.2-6.3 7.2-10z" class="st75"/>
          <linearGradient id="SVGID_46_" x1="299.684" x2="348.903" y1="342.676" y2="342.676" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00074d"/>
            <stop offset=".03" stop-color="#00074d"/>
            <stop offset=".464" stop-color="#00074d"/>
            <stop offset=".55" stop-color="#143da1"/>
            <stop offset=".99" stop-color="#143da1"/>
          </linearGradient>
          <path fill="url(#SVGID_46_)" d="M1052.1 331.2v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10v-8.7h-49.2z"/>
          <path d="M1059.3 321.2c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0s9.6-14.5 0-20.1c-9.6-5.5-25.1-5.5-34.8 0z" class="st76"/>
          <linearGradient id="SVGID_47_" x1="270.796" x2="315.414" y1="331.244" y2="331.244" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset=".01" stop-color="#143da1"/>
            <stop offset=".45" stop-color="#143da1"/>
            <stop offset=".536" stop-color="#00074d"/>
            <stop offset=".97" stop-color="#00074d"/>
            <stop offset="1" stop-color="#1f2243"/>
          </linearGradient>
          <path fill="url(#SVGID_47_)" d="M1054.4 331.2c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9-6.2 0-11.9 1.4-16.2 3.9-3.9 2.3-6.1 5.2-6.1 8.1z"/>
          <path d="M1054.8 333.4c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          <linearGradient id="SVGID_48_" x1="1043.128" x2="1092.346" y1="331.244" y2="331.244" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_48_)" d="M1076.7 317.5c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6 0 3.6-2.5 7-7 9.6-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.7-2.6 10.8-4.1 17.2-4.1m0-.5c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.8-11.1-4.2-17.4-4.2z"/>
          <linearGradient id="SVGID_49_" x1="299.684" x2="348.903" y1="329.062" y2="329.062" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00074d"/>
            <stop offset=".03" stop-color="#00074d"/>
            <stop offset=".464" stop-color="#00074d"/>
            <stop offset=".55" stop-color="#143da1"/>
            <stop offset=".99" stop-color="#143da1"/>
          </linearGradient>
          <path fill="url(#SVGID_49_)" d="M1052.1 317.6v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10v-8.7h-49.2z"/>
          <path d="M1059.3 307.6c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0s9.6-14.5 0-20.1c-9.6-5.6-25.1-5.6-34.8 0z" class="st76"/>
          <linearGradient id="SVGID_50_" x1="270.796" x2="315.414" y1="317.631" y2="317.631" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
            <stop offset=".01" stop-color="#143da1"/>
            <stop offset=".45" stop-color="#143da1"/>
            <stop offset=".536" stop-color="#00074d"/>
            <stop offset=".97" stop-color="#00074d"/>
            <stop offset="1" stop-color="#1f2243"/>
          </linearGradient>
          <path fill="url(#SVGID_50_)" d="M1054.4 317.6c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9-6.2 0-11.9 1.4-16.2 3.9-3.9 2.3-6.1 5.2-6.1 8.1z"/>
          <path d="M1054.8 319.8c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          <linearGradient id="SVGID_51_" x1="1043.128" x2="1092.346" y1="317.631" y2="317.631" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_51_)" d="M1076.7 303.9c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6 0 3.6-2.5 7-7 9.6-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.7-2.7 10.8-4.1 17.2-4.1m0-.5c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.8-11.1-4.2-17.4-4.2z"/>
          <g>
            <linearGradient id="SVGID_52_" x1="299.684" x2="348.903" y1="315.449" y2="315.449" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".03" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#00074d"/>
              <stop offset=".55" stop-color="#143da1"/>
              <stop offset=".99" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_52_)" d="M1052.1 304v8.7c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.8-2.8 7.2-6.4 7.2-10V304h-49.2z"/>
            <path d="M1059.3 294c-9.6 5.5-9.6 14.5 0 20.1 9.6 5.5 25.2 5.5 34.8 0s9.6-14.5 0-20.1c-9.6-5.6-25.1-5.6-34.8 0z" class="st76"/>
            <linearGradient id="SVGID_53_" x1="285.784" x2="330.401" y1="304.017" y2="304.017" gradientTransform="matrix(-1 0 0 1 1383.556 0)" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_53_)" d="M1054.4 304c0 .7.1 1.5.4 2.2.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9.3-.7.4-1.4.4-2.2 0-2.9-2.2-5.8-6.1-8.1-4.3-2.5-10.1-3.9-16.3-3.9-6.2 0-11.9 1.4-16.2 3.9-3.9 2.3-6.1 5.2-6.1 8.1z"/>
            <path d="M1054.8 306.2c.8 2.2 2.7 4.2 5.7 5.9 4.3 2.5 10.1 3.9 16.2 3.9 6.2 0 11.9-1.4 16.3-3.9 2.9-1.7 4.9-3.7 5.7-5.9-1.1-1.3-2.6-2.5-4.5-3.6-9.6-5.5-25.2-5.5-34.8 0-1.9 1.1-3.4 2.3-4.6 3.6z" class="st76"/>
          </g>
          <linearGradient id="SVGID_54_" x1="1043.128" x2="1092.346" y1="304.018" y2="304.018" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#71cbd5"/>
            <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
          </linearGradient>
          <path fill="url(#SVGID_54_)" d="M1076.7 290.3c6.5 0 12.6 1.5 17.2 4.1 4.5 2.6 7 6 7 9.6s-2.5 7-7 9.6c-4.6 2.6-10.7 4.1-17.2 4.1s-12.6-1.5-17.2-4.1c-4.5-2.6-7-6-7-9.6 0-3.6 2.5-7 7-9.6 4.7-2.7 10.8-4.1 17.2-4.1m0-.5c-6.3 0-12.6 1.4-17.4 4.2-9.6 5.5-9.6 14.5 0 20.1 4.8 2.8 11.1 4.2 17.4 4.2 6.3 0 12.6-1.4 17.4-4.2 9.6-5.5 9.6-14.5 0-20.1-4.8-2.8-11.1-4.2-17.4-4.2z"/>
          <path d="M1101.4 331.2v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10v-2.2c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.7 7.2-6.3 7.2-10z" class="st75"/>
          <path d="M1101.4 317.6v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10v-2.2c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.7 7.2-6.3 7.2-10z" class="st75"/>
          <path d="M1101.4 304v2.2c0 3.6-2.4 7.3-7.2 10-9.6 5.6-25.2 5.6-34.8 0-4.8-2.8-7.2-6.4-7.2-10V304c0 3.6 2.4 7.3 7.2 10 9.6 5.6 25.2 5.6 34.8 0 4.7-2.7 7.2-6.3 7.2-10z" class="st75"/>
          <g>
            <linearGradient id="SVGID_55_" x1="825.971" x2="763.612" y1="224.91" y2="116.902" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#1f2243"/>
              <stop offset=".03" stop-color="#00074d"/>
              <stop offset=".31" stop-color="#00074d"/>
              <stop offset=".409" stop-color="#143da1"/>
              <stop offset=".439" stop-color="#143da1"/>
              <stop offset=".528" stop-color="#00074d"/>
              <stop offset=".738" stop-color="#00074d"/>
              <stop offset=".793" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_55_)" d="M828.9 161.1l.1 29.9c.1 25.3-14.1 39.8-36.7 42.7-22.8-28.9-47.1-108.9-47.1-108.9l9.8-6.1c1.8 1.3 3.6 2.5 5.4 3.5.5.3 1 .5 1.4.8 7.5 4 14.4 5.4 20.4 4.5 3.7-.5 7-1.9 9.8-4.2 2.8 5.4 6.1 10.6 9.8 15.4 6 7.7 12.9 14.2 20.4 18.8l1.5.9c1.7 1.1 3.5 2 5.2 2.7z"/>
            <linearGradient id="SVGID_56_" x1="825.971" x2="763.612" y1="224.91" y2="116.902" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#1f2243"/>
              <stop offset=".03" stop-color="#00074d"/>
              <stop offset=".31" stop-color="#00074d"/>
              <stop offset=".409" stop-color="#143da1"/>
              <stop offset=".439" stop-color="#143da1"/>
              <stop offset=".528" stop-color="#00074d"/>
              <stop offset=".738" stop-color="#00074d"/>
              <stop offset=".793" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_56_)" d="M828.9 161.1l.1 29.9c.1 25.3-14.1 39.8-36.7 42.7-22.8-28.9-47.1-108.9-47.1-108.9l9.8-6.1c1.8 1.3 3.6 2.5 5.4 3.5.5.3 1 .5 1.4.8 7.5 4 14.4 5.4 20.4 4.5 3.7-.5 7-1.9 9.8-4.2 2.8 5.4 6.1 10.6 9.8 15.4 6 7.7 12.9 14.2 20.4 18.8l1.5.9c1.7 1.1 3.5 2 5.2 2.7z"/>
            <linearGradient id="SVGID_57_" x1="752.761" x2="756.723" y1="115.502" y2="121.115" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_57_)" d="M828.9 161.1l.1 29.9c.1 25.3-14.1 39.8-36.7 42.7-22.8-28.9-47.1-108.9-47.1-108.9l9.8-6.1c1.8 1.3 3.6 2.5 5.4 3.5.5.3 1 .5 1.4.8 7.5 4 14.4 5.4 20.4 4.5 3.7-.5 7-1.9 9.8-4.2 2.8 5.4 6.1 10.6 9.8 15.4 6 7.7 12.9 14.2 20.4 18.8l1.5.9c1.7 1.1 3.5 2 5.2 2.7z"/>
            <path d="M819 167.4v29.9c0 25.3-14.2 39.8-36.9 42.5-22.7-28.9-36.9-59.9-36.9-85.1v-29.9c1.8 1.3 3.5 2.5 5.4 3.5.5.3 1 .5 1.4.8 7.5 4 14.4 5.5 20.4 4.6 3.5-.5 6.7-1.8 9.4-3.8.1-.1.3-.2.4-.3.1.1.1.3.2.4 2.7 5.3 5.9 10.3 9.5 15 5.9 7.7 12.9 14.3 20.4 18.9.5.3 1 .6 1.4.9 1.7 1 3.5 1.9 5.3 2.6z" class="st76"/>
            <g>
              <path d="M781 231.7c-19.7-26.3-29.9-52.9-29.9-73.6v-22.6c.3.2.7.4 1 .5 8.3 4.5 16.3 6.3 23.3 5.3 2.3-.3 4.6-1 6.6-1.9 2.1 3.3 4.3 6.5 6.6 9.5 7 9.1 15 16.5 23.3 21.6.3.2.7.4 1 .6v22.6c.1 20.8-12.2 34.5-31.9 38z" class="st75"/>
            </g>
            <g opacity=".5">
              <path d="M782.3 239.3l-.2.5c-22.7-28.9-36.9-59.9-36.9-85.1v-29.9c1.8 1.3 3.5 2.5 5.4 3.5.5.3 1 .5 1.4.8 7.5 4 14.4 5.5 20.4 4.6 3.7-.5 7-1.9 9.8-4.1l-.2.7c-2.8 2.1-6 3.4-9.6 3.9-6.2.9-13.3-.7-20.6-4.7-.5-.3-1-.5-1.4-.8-1.6-.9-3.2-1.9-4.7-3v29c-.1 24.1 13.2 54.8 36.6 84.6z" class="st14"/>
            </g>
            <g class="st47">
              <path d="M819 167.4v29.9c0 25.3-14.2 39.8-36.9 42.5l.2-.5c23.5-2.8 36.2-17.8 36.2-42v-29.6c-1.6-.7-3.3-1.5-5.1-2.6l-1.5-.9c-7.3-4.5-14.3-11-20.5-19-3.7-4.8-6.7-9.5-9.6-15l.2-.7c.1.1.1.3.2.4 2.7 5.3 5.9 10.3 9.5 15 5.9 7.7 12.9 14.3 20.4 18.9.5.3 1 .6 1.4.9 1.9 1.1 3.7 2 5.5 2.7z" class="st2"/>
            </g>
            <g>
              <path d="M768.8 170.1v24.1l26.6 15.4v-24.1l-26.6-15.4z" class="st0"/>
              <path d="M774.5 178.4l-2.9-1.7v-10c0-6.7 4.7-9.4 10.5-6.1s10.5 11.5 10.5 18.2l-2.9-1.7c0-4.9-3.4-10.8-7.7-13.3s-7.7-.5-7.7 4.4v10.2z" class="st0"/>
              <path d="M784.9 197.2l-5.5-3.1.8-5.6c-.5-.9-.8-1.9-.8-2.8 0-1.7 1.2-2.4 2.7-1.6 1.5.9 2.7 3 2.7 4.7 0 .9-.3 1.5-.9 1.8l1 6.6z" class="st76"/>
              <path d="M784 190.6l.5 4-3.1-1.8.8-5.6c-.5-.9-.8-1.9-.8-2.8 0-.2 0-.5.1-.7.2.1.4.1.6.3 1.5.9 2.7 3 2.7 4.7.1 1-.3 1.6-.8 1.9z" class="st2"/>
              <path d="M792.7 183.9l-2.9-1.6v-5l2.9 1.6z" class="st0"/>
            </g>
          </g>
          <g id="Domino_6_1_">
            <path d="M747.1 258.2l-7 4.1-17.7-10.2 7-4.1z" class="st2"/>
            <path d="M764.8 268.4l-7 4.1-17.7-10.2 7-4.1z" class="st2"/>
            <path d="M764.8 268.4l-7 4.1v-61.8l7-4z" class="st2"/>
            <path d="M757.8 210.7v61.8l-35.4-20.4v-61.8z" class="st76"/>
            <linearGradient id="SVGID_58_" x1="738.441" x2="780.76" y1="198.5" y2="198.5" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </linearGradient>
            <path fill="url(#SVGID_58_)" d="M764.8 206.7l-7 4-35.4-20.4 7-4z"/>
            <linearGradient id="SVGID_59_" x1="703.642" x2="738.97" y1="221.189" y2="221.189" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_59_)" d="M722.9 251.8l-.5.3v-61.8l35.4 20.4-.5.3-34.4-19.9z"/>
            <g class="st47">
              <path d="M757.8 272.5l-35.4-20.4.5-.3 34.4 19.8V211l.5-.3z" class="st2"/>
            </g>
          </g>
          <g id="Domino_5_1_">
            <path d="M721.4 274.9l-6.9 4-17.7-10.2 7-4zm17.7 10.2l-7 4-17.6-10.2 6.9-4z" class="st2"/>
            <path d="M739.1 285.1l-7 4v-61.7l7-4.1z" class="st2"/>
            <path d="M732.1 227.4v61.7l-35.3-20.4V207z" class="st76"/>
            <linearGradient id="SVGID_60_" x1="711.213" x2="753.537" y1="215.172" y2="215.172" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </linearGradient>
            <path fill="url(#SVGID_60_)" d="M732.1 227.4l7-4-35.3-20.4-7 4z"/>
            <linearGradient id="SVGID_61_" x1="680.397" x2="715.72" y1="237.851" y2="237.851" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_61_)" d="M731.6 227.6l-34.3-19.8v60.6l-.5.3V207l35.3 20.4z"/>
            <g class="st47">
              <path d="M732.1 289.1l-35.3-20.4.5-.3 34.3 19.9v-60.7l.5-.2z" class="st2"/>
            </g>
          </g>
          <g id="Domino_4_1_">
            <path d="M690.6 295.4l-6.7 1.6-17.7-10.2 6.7-1.6zm17.7 10.2l-6.8 1.6-17.6-10.2 6.7-1.6z" class="st2"/>
            <path d="M708.3 305.6l-6.8 1.6 14.8-67.8 6.7-1.7z" class="st2"/>
            <path d="M716.3 239.4l-14.8 67.8-35.3-20.4 14.7-67.8z" class="st76"/>
            <linearGradient id="SVGID_62_" x1="694.112" x2="744.309" y1="228.33" y2="228.33" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </linearGradient>
            <path fill="url(#SVGID_62_)" d="M716.3 239.4l6.7-1.7-35.3-20.4-6.8 1.7z"/>
            <linearGradient id="SVGID_63_" x1="638.576" x2="688.64" y1="252.885" y2="252.885" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_63_)" d="M666.2 286.8l14.7-67.8 35.4 20.4-.6.2-34.4-19.9-14.6 66.9z"/>
            <g class="st111">
              <path d="M678.1 268c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8.8.4 1.2 1.5 1 2.4-.3.9-1.1 1.2-1.8.8m3.5 2.1c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8s1.2 1.5 1 2.4c-.2.8-1 1.2-1.8.8m3.6 2c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8.8.4 1.2 1.5 1 2.4-.2.9-1 1.3-1.8.8m3.6 2.1c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8.8.4 1.2 1.5 1 2.4-.3.9-1 1.2-1.8.8m3.6 2c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8.8.4 1.2 1.5 1 2.4-.3.9-1.1 1.3-1.8.8m3.5 2.1c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8.8.4 1.2 1.5 1 2.4-.2.9-1 1.3-1.8.8m3.6 2.1c-.8-.4-1.2-1.5-1-2.4.2-.9 1-1.2 1.8-.8s1.2 1.5 1 2.4c-.2.9-1 1.2-1.8.8m4.4-3.6l-27.8-16c-.5-.3-1.1-.1-1.2.6l-1.2 5.4c-.1.6.2 1.4.7 1.7l27.8 16c.5.3 1.1.1 1.2-.6l1.2-5.4c.2-.7-.2-1.4-.7-1.7m-9.8-32.7c-.6-.1-.9.2-1 .7-.1.4 0 .8.6 1.3l.4-2m1.2.4c-.1 0-.2-.1-.2-.1l-.5 2.3.7.5.5-2.3c-.1-.2-.3-.3-.5-.4m-1.1 3.9l-.6 2.5c.1.1.3.2.5.3.1.1.2.1.2.1l.5-2.4-.6-.5m1.6 1.2l-.5 2.1c.7.1 1-.2 1.1-.7.2-.4.1-.9-.6-1.4" class="st75"/>
              <path d="M696.2 243.1l.3-1.1 1 .6-.3 1.3c1.5 1.3 2 2.9 1.8 4.1 0 .1-.2.2-.3.1l-1.3-.7c-.1-.1-.2-.2-.2-.4 0-.4 0-.8-.4-1.3l-.5 2.2c1.9 1.3 2.4 2.8 2.1 4.1-.3 1.3-1.4 2.2-3.4 1.6l-.3 1.3-1-.6.3-1.1-.2-.1c-.2-.1-.3-.2-.5-.3l-.3 1.1-.9-.5.3-1.3c-1.6-1.4-2.2-3.1-1.9-4.3v-.1c0-.2.2-.2.3-.1l1.2.7c.2.1.2.2.2.4 0 .5.1 1 .5 1.6l.5-2.3c-1.6-1.2-2.3-2.5-2-4 .3-1.4 1.5-2.2 3.3-1.6l.3-1.3.9.5-.3 1.1.2.1c.3.1.4.2.6.3m.7-5c-4.7-2.7-9.4-.5-10.6 4.9-1.2 5.4 1.6 11.9 6.3 14.6 4.7 2.7 9.4.5 10.6-4.9 1.1-5.4-1.7-11.9-6.3-14.6" class="st75"/>
            </g>
            <g class="st47">
              <path d="M701.5 307.2l-35.3-20.4.5-.2 34.5 19.9 14.5-66.9.6-.2z" class="st2"/>
            </g>
          </g>
          <g id="Domino_3_1_">
            <path d="M651.2 319.8l-5.3-2.1-17.7-10.2 5.3 2.1zm17.7 10.2l-5.4-2.1-17.6-10.2 5.3 2.1z" class="st2"/>
            <path d="M697.9 260.7l-34.4 67.2-35.3-20.4 34.4-67.2z" class="st76"/>
            <linearGradient id="SVGID_64_" x1="676.558" x2="717.245" y1="251.589" y2="251.589" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </linearGradient>
            <path fill="url(#SVGID_64_)" d="M703.2 262.8l-5.3-2.1-35.3-20.4 5.3 2.1z"/>
            <path d="M668.9 330l-5.4-2.1 34.4-67.2 5.3 2.1z" class="st2"/>
            <g>
              <linearGradient id="SVGID_65_" x1="614.419" x2="676.095" y1="271.229" y2="276.566" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#71cbd5"/>
                <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
              </linearGradient>
              <path fill="url(#SVGID_65_)" d="M628.8 307.3l-.6.2 34.4-67.2.2.2 35.1 20.2-.6.2-34.6-19.9z"/>
            </g>
            <g>
              <g class="st111">
                <path d="M664.6 255.8l-6.8 12.8 11.3-.9-11.3.9 6.8-12.8" class="st75"/>
                <path d="M664.6 255.8l-6.8 12.8 11.3-.9-4.5-11.9m12.6 24.3l-18.5-10.6 18.5 10.6m8-12.5l-12.1 2.4 5.3 10.4 6.8-12.8m-8 12.5l-18.5-10.6 18.5 10.6m-5.1-9.9l-1.9.4-.8-1.9-10.7.8 18.5 10.6-5.1-9.9" class="st75"/>
                <path d="M684.6 266.7l-18.9-10.9 4.4 11.9.1.4.2.5.4.9.9-.2.4-.1.5-.1 12-2.4" class="st75"/>
              </g>
            </g>
            <g class="st47">
              <path d="M663.5 327.9l-35.3-20.4.6-.2 34.5 19.9 34-66.3.6-.2z" class="st2"/>
            </g>
          </g>
          <g id="Domino_2_1_">
            <path d="M609.9 340.8l-3.5-5-17.7-10.2 3.5 5z" class="st2"/>
            <path d="M627.5 351l-3.5-5-17.6-10.2 3.5 5z" class="st2"/>
            <path d="M627.5 351l-3.5-5 46.3-57.6 3.5 5z" class="st2"/>
            <path d="M670.3 288.4L624 346l-35.3-20.4L635 268z" class="st76"/>
            <g>
              <linearGradient id="SVGID_66_" x1="585.571" x2="635.279" y1="283.05" y2="309.143" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#71cbd5"/>
                <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
              </linearGradient>
              <path fill="url(#SVGID_66_)" d="M589.4 325.5l-.7.1L635 268l.2.1 35.1 20.3-.3.4-34.9-20.1z"/>
            </g>
            <g>
              <g class="st111">
                <path d="M612.1 309.3c-.8-.4-.9-1.4-.3-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.3 2.2-.6.8-1.7 1.1-2.5.6m3.6 2.1c-.8-.4-.9-1.4-.3-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.2 2.2-.5.8-1.6 1-2.4.6m3.6 2.1c-.8-.4-.9-1.4-.3-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.2 2.2-.5.8-1.7 1-2.4.6m3.5 2c-.8-.4-.9-1.4-.3-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.2 2.2-.5.8-1.6 1.1-2.4.6m3.6 2.1c-.8-.4-.9-1.4-.2-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.3 2.2-.7.8-1.8 1-2.6.6m3.6 2.1c-.8-.4-.9-1.4-.2-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.3 2.2-.7.8-1.8 1-2.6.6m3.6 2c-.8-.4-.9-1.4-.3-2.2.6-.8 1.8-1 2.5-.6.8.4.9 1.4.2 2.2-.5.8-1.7 1.1-2.4.6m7-2.8l-27.8-16c-.5-.3-1.3-.1-1.8.4l-3.8 4.6c-.4.5-.4 1.2.2 1.5l27.8 16c.5.3 1.3.1 1.8-.4l3.8-4.6c.4-.5.3-1.2-.2-1.5m-1.4-23c-1.5-.9-1.7-2.8-.5-4.2 1.2-1.5 3.4-2 4.9-1.1 1.5.9 1.7 2.8.5 4.2-1.2 1.5-3.4 2-4.9 1.1m8.2-9.8c-.1-.1-.3-.1-.4-.2-3.9-2-9.5-.8-12.8 2.8-1.7 1.9-2.2 3.7-2.5 6l-.9 10.8c0 .1 0 .2.1.2s.2.1.3 0l11.9-4.5c2.2-.9 4.4-2.1 5.9-4.1 2.9-3.8 2.3-8.6-1.3-11-.1.1-.2 0-.3 0" class="st75"/>
              </g>
            </g>
            <g class="st47">
              <path d="M624 346l-35.3-20.4.7-.1 34.5 19.9 45.7-56.8.7-.2z" class="st2"/>
            </g>
          </g>
          <g id="Domino_1_1_">
            <path d="M564.1 366.4l-2.4-6.2L544 350l2.4 6.2z" class="st2"/>
            <path d="M581.7 376.6l-2.4-6.2-17.6-10.2 2.4 6.2z" class="st2"/>
            <path d="M581.7 376.6l-2.4-6.2 50.3-50.1 2.4 6.2z" class="st2"/>
            <path d="M629.6 320.3l-50.3 50.1L544 350l50.3-50.1z" class="st76"/>
            <g>
              <g class="st111">
                <path d="M595.3 326.8c-1.6-.9-1.7-2.8-.4-4.1 1.4-1.4 3.7-1.7 5.3-.8 1.6.9 1.8 2.8.4 4.1-1.4 1.3-3.8 1.7-5.3.8m8.6-8.8c-2.5-1.5-5.7-1.9-8.8-1.4-3.1.5-6.1 1.9-8.2 4-2.8 2.8-3.5 6.2-2.4 9.1l.8-.8c1.2-1.2 2.9-2 4.6-2.3 1.7-.3 3.5 0 4.9.8 2.8 1.6 3.2 5 .7 7.4l-.5.5c.6 0 1.3-.1 1.9-.2 3.1-.5 6.1-1.9 8.2-4 2.2-2.2 3.1-4.7 2.9-7.1-.2-2.3-1.6-4.5-4.1-6m-38.4 21.6c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.8 1.3.2 2c-.7.7-1.9.9-2.6.4m3.5 2.1c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.8 1.3.2 2c-.6.6-1.8.8-2.6.4m3.6 2c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.9 1.3.2 2-1.8.9-2.6.4m3.6 2.1c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.9 1.3.2 2-1.8.9-2.6.4m3.6 2.1c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.8 1.3.2 2c-.7.6-1.9.8-2.6.4m3.5 2c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.8 1.3.2 2c-.6.7-1.8.9-2.6.4m3.6 2.1c-.8-.4-.9-1.3-.2-2s1.8-.8 2.6-.4.9 1.3.2 2-1.8.8-2.6.4m7.2-2.2l-27.8-16c-.5-.3-1.4-.2-1.8.3l-4 4c-.5.5-.4 1.1.1 1.4l27.8 16c.5.3 1.4.2 1.8-.3l4-4c.5-.5.5-1.1-.1-1.4" class="st75"/>
              </g>
            </g>
            <g>
              <linearGradient id="SVGID_67_" x1="554.274" x2="591.265" y1="316.488" y2="337.562" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#71cbd5"/>
                <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
              </linearGradient>
              <path fill="url(#SVGID_67_)" d="M544.8 349.9l-.8.1 50.3-50.1.1.1 35.2 20.3-.8.1-34.5-19.9z"/>
            </g>
            <g>
              <linearGradient id="SVGID_68_" x1="544.017" x2="629.584" y1="345.351" y2="345.351" gradientUnits="userSpaceOnUse">
                <stop offset=".01" stop-color="#00074d"/>
                <stop offset=".536" stop-color="#00074d"/>
                <stop offset="1" stop-color="#1f2243"/>
              </linearGradient>
              <path fill="url(#SVGID_68_)" d="M579.3 370.4l-.1-.1L544 350l.8-.1 34.5 19.9 49.5-49.4.8-.1z" opacity=".3"/>
            </g>
          </g>
          <g>
            <g class="st20">
              <path d="M896.3 250.6c0 15.4 21.2 31.7 47.4 36.3 26.2 4.6 47.4-4.2 47.4-19.6s-21.2-31.7-47.4-36.3c-26.2-4.6-47.4 4.2-47.4 19.6z" class="st2 shadow5"/>
            </g>
            <radialGradient id="SVGID_70_" cx="703.95" cy="298.308" r="53" gradientTransform="matrix(-1 -.1763 0 1 1607.895 62.337)" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </radialGradient>
            <path fill="url(#SVGID_70_)" d="M901.6 233.4v.6c.6 13.4 19.4 27.5 42.6 31.6 23.1 4.1 41.9-3.3 42.5-16.6v-1.2c-.3-23.3-19.3-45.3-42.5-49.4-23.3-4.1-42.2 11.3-42.6 34.4v.6z"/>
            <linearGradient id="SVGID_71_" x1="881.701" x2="925.534" y1="202.315" y2="223.603" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_71_)" d="M936.7 197.7v.5c2.4 0 4.9.2 7.4.7 11.1 2 21.7 8.1 29.6 17.4 7.9 9.2 12.4 20.4 12.6 31.6v1.2c-.5 10.3-12.4 17.2-29.7 17.2-4 0-8.1-.4-12.3-1.1-11.1-2-21.6-6.3-29.5-12.2-7.9-5.8-12.4-12.6-12.6-18.9v-1.2c.3-20.1 14.9-34.6 34.6-34.6v-.6m-.1 0c-19.8 0-34.8 14.4-35.1 35.1v1.2c.6 13.4 19.4 27.5 42.6 31.6 4.3.8 8.5 1.1 12.4 1.1 17.1 0 29.7-6.9 30.2-17.7v-1.2c-.3-23.3-19.3-45.3-42.5-49.4-2.7-.5-5.2-.7-7.6-.7z"/>
            <radialGradient id="SVGID_72_" cx="671.487" cy="187.655" r="22.798" gradientTransform="matrix(-1 0 0 1 1607.895 0)" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </radialGradient>
            <circle cx="946.3" cy="184" r="22.8" fill="url(#SVGID_72_)"/>
          </g>
          <g>
            <path d="M936.3 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.3 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M936.2 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M936.2 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936.1 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936 191.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M936 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V182c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M936 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M936 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.9 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.8 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.7 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36C940 210 945 202.9 945 193v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.6 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.5 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.4 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.3 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.2 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.1 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.1 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935.1 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M935.1 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M935.1 191.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M935.1 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M935.1 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M935.1 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M935.1 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M935 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.9 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.9 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.9 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.9 191.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V192zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.9V176c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.9 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V192zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.8 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.7 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.6 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.5 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M934.4 192.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M934.4 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M934.4 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.3 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V183c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.2 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934.2 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934.1 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934 192.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M934 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M934 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M934 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M934 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.9 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.8 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.7 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.6 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.5 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.4 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M933.3 192.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M933.3 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M933.3 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.2 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.1 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.1 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.1 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.1 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.1 192.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V193zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.1 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933.1 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V177c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933.1 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M933.1 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M933.1 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4V193zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M933 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.9 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.8 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.7 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.4z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.6 193.2c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.5 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.3c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.4 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6V184c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5L922 198v-4.6zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5L922 198v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.3 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5L922 198v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.4c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.2 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932.1 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932 193.5c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M932 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M932 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M932 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.9 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.6c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.8 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.7 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.7c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.6 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-.9-3-2.8-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.5 193.8c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.5 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.4z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.3-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zM917 183c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.4 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.8-3-4.3zm-6.4-15.8V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 193.9c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.3 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9V178c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.3 1.7-3 .8zm-10.2 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4V194zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.2 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.4-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.7-.9-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.2.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-11c0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3-1.3 1.8-3 .8-3-2.8-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.8-3 .8zm-10.3.1l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.1-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3 0 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.3-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.1-.4-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.9v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.1-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8c-1.6-1-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.1-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.3-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3-.1 1.3-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-4.2-10.9c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3zm-6.4-15.8v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3z" class="st2"/>
            <path d="M931.1 194.1c-1.6-.9-3-2.9-3-4.3s1.3-1.8 3-.8c1.6.9 3 2.9 3 4.3s-1.4 1.7-3 .8zm-10.3 0l9.4 5.4v4.5l-9.4-5.4v-4.5zm-10.6-26.7v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-3.4.4-7.3-.5-11.4-2.6-.2-.2-.3-.2-.5-.3zm6.4 15.8c0-1.4 1.3-1.8 3-.8 1.6.9 3 2.9 3 4.3s-1.3 1.8-3 .8-3-2.9-3-4.3z" class="st75"/>
            <linearGradient id="SVGID_73_" x1="924.682" x2="928.153" y1="200.438" y2="194.426" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </linearGradient>
            <path fill="url(#SVGID_73_)" d="M930.1 199.5v4.6l-9.3-5.5v-4.5z"/>
            <radialGradient id="SVGID_74_" cx="933.043" cy="193.571" r="7.94" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </radialGradient>
            <path fill="url(#SVGID_74_)" d="M934 193.2c0 1.4-1.3 1.8-3 .8-1.6-.9-3-2.8-3-4.2s1.3-1.8 3-.8c1.7.9 3 2.8 3 4.2z"/>
            <radialGradient id="SVGID_75_" cx="921.101" cy="187.761" r="8.924" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#1f2243"/>
            </radialGradient>
            <path fill="url(#SVGID_75_)" d="M922.5 186.6c0 1.4-1.3 1.8-3 .8-1.6-.9-3-2.8-3-4.3 0-1.4 1.3-1.8 3-.8s3 2.9 3 4.3z"/>
            <g>
              <linearGradient id="SVGID_76_" x1="903.066" x2="939.789" y1="188.288" y2="194.026" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#71cbd5"/>
                <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
              </linearGradient>
              <path fill="url(#SVGID_76_)" d="M910.7 168.1c3.3 1.8 6.7 2.7 9.7 2.7.6 0 1.2 0 1.8-.1 1-.1 2.1-.4 3-.8.9 1.5 2 2.9 3 4.3 3.4 4.4 7.4 8.1 11.5 10.6.1.1.2.1.3.2v10.6c0 9.9-5.1 16.5-13.9 18.1-9.6-12.8-15.4-26-15.4-35.5v-10.1m10.5 20.3c1.1 0 1.8-.7 1.8-1.8 0-1.6-1.4-3.6-3.2-4.7-.7-.4-1.3-.6-1.9-.6-1.1 0-1.8.7-1.8 1.8 0 1.6 1.4 3.6 3.2 4.7.7.4 1.3.6 1.9.6m11.5 6.7c1.1 0 1.8-.7 1.8-1.8 0-1.6-1.4-3.6-3.2-4.7-.7-.4-1.3-.6-1.9-.6-1.1 0-1.8.7-1.8 1.8 0 1.6 1.4 3.6 3.2 4.7.7.4 1.3.6 1.9.6m-2.1 9.8v-5.7l-.2-.1-9.4-5.4-.7-.4v5.6l.2.1 9.4 5.4.7.5m-20.4-37.5v10.8c0 9.9 6 23.3 15.6 36 9.6-1.6 14.6-8.7 14.6-18.6v-10.8c-.2-.1-.3-.2-.5-.3-4-2.5-8-6.1-11.4-10.5-1.1-1.5-2.2-3-3.2-4.6-1 .4-2.1.7-3.2.9-.6.1-1.1.1-1.7.1-3 0-6.3-.9-9.7-2.8-.2-.1-.3-.1-.5-.2zm11 20.6c-.5 0-1-.2-1.6-.5-1.6-.9-3-2.9-3-4.3 0-.9.5-1.4 1.3-1.4.5 0 1 .2 1.6.5 1.6.9 3 2.9 3 4.3 0 .9-.5 1.4-1.3 1.4zm11.5 6.6c-.5 0-1-.2-1.6-.5-1.6-.9-3-2.9-3-4.3 0-.9.5-1.4 1.3-1.4.5 0 1 .2 1.6.5 1.6.9 3 2.9 3 4.3 0 .9-.5 1.4-1.3 1.4zm-2.6 9.4l-9.4-5.4v-4.5l9.4 5.4v4.5z"/>
            </g>
          </g>
          <g>
            <path d="M440.4 487.4c-.1 1.3.5 2.5 2.1 3.4L569.2 564c3.1 1.8 8.4 1.5 12-.5l85-49.1c2.1-1.2 3.1-2.8 2.9-4.3v-5l-228.7-23v5.3zM722.8 401.7l2.9 1.7c3.3 1.9 4.4 6.1 2.5 9.4l-49.6 85.9c-1.9 3.3-6.1 4.4-9.4 2.5l-2.9-1.7 56.5-97.8z" class="st2"/>
            <path d="M569.2 558.5l-126.7-73.1c-3.1-1.8-2.7-4.9.9-6.9l85-49.1c3.5-2 8.9-2.3 12-.5L667.1 502c3.1 1.8 2.7 4.9-.9 6.9l-85 49.1c-3.5 2.1-8.9 2.3-12 .5z" class="st76"/>
            <path fill="#1d6f8c" d="M669.1 512.3v-.3c.1.1.1.2 0 .3z"/>
            <path d="M501.7 507.4l28.8 16.5c.7.4 1.8.3 2.6-.2l20.4-11.8c.8-.5.9-1.2.2-1.5l-28.8-16.5c-.7-.4-1.8-.3-2.6.2l-20.4 11.8c-.8.4-.9 1.1-.2 1.5z" class="st75"/>
            <linearGradient id="SVGID_77_" x1="420.844" x2="629.842" y1="414.791" y2="414.791" gradientTransform="matrix(-1.0041 -.00423 -.0042 .9973 1153.716 2.987)" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#fbfcff" stop-opacity="0"/>
              <stop offset=".338" stop-color="#fff"/>
              <stop offset=".657" stop-color="#fff"/>
              <stop offset=".995" stop-color="#fbfcff" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_77_)" d="M543 420.8l40.5 23.5 23.8 13.8 2.9 1.7 15.6 9 10.9 6.3 2.7 1.6 5.3 3.1 24.3 14.1c.1 0 .1 0 .1-.1l43.7-74.5 6.4-10.9v-.2l-9-5.2-1.2-.7-63.9-37-5.3-3.1-2.7-1.6-29.4-17-14.5-8.4h-.1l-9.2 15.7-29.7 50.7-11.2 19v.2z"/>
            <linearGradient id="SVGID_78_" x1="420.844" x2="629.842" y1="414.791" y2="414.791" gradientTransform="matrix(-1.0041 -.00423 -.0042 .9973 1153.716 2.987)" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#fbfcff" stop-opacity="0"/>
              <stop offset=".338" stop-color="#fff"/>
              <stop offset=".657" stop-color="#fff"/>
              <stop offset=".995" stop-color="#fbfcff" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_78_)" d="M543 420.8l40.5 23.5 23.8 13.8 2.9 1.7 15.6 9 10.9 6.3 2.7 1.6 5.3 3.1 24.3 14.1c.1 0 .1 0 .1-.1l43.7-74.5 6.4-10.9v-.2l-9-5.2-1.2-.7-63.9-37-5.3-3.1-2.7-1.6-29.4-17-14.5-8.4h-.1l-9.2 15.7-29.7 50.7-11.2 19v.2z"/>
            <path d="M633 507.5l-2.8-1.6-5.1-3-3-1.7-5.3-3-2.8-1.7-5.3-3-13.7-7.9-2.5-1.5-10.9-6.3-5.3-3-11-6.4-5.3-3-10.9-6.3-5.2-3.1-2.9-1.6-5.2-3-2.7-1.6-10-5.7 6.3-3.7 10 5.8 2.7 1.5 1.6.9 6.5 3.8 5.3 3 2.8 1.7 3.4 1.9 12.8 7.4 5.3 3.1 4.4 2.5 3.7 2.2 2.9 1.6 5.2 3 2.8 1.7 5.3 3 2.8 1.6 5.1 3 19.3 11.1.2.1 9.3 5.4 4.4 2.5-6.4 3.7-5.8-3.4zm-11.8 2.2l-2.9-1.6-5.2-3.1-2.9-1.6-5.3-3.1-2.8-1.6-5.3-3-2.7-1.6-5.3-3.1-2.9-1.7-5.3-3-2.8-1.6-5.2-3.1-2.9-1.6-5.2-3-2.9-1.7-5.2-3-2.9-1.7-5.3-3-10.9-6.3-5.3-3.1-2.8-1.6-5.3-3.1-3.7-2.1-4.9-2.8 6.3-3.7 5.8 3.4 2.8 1.6 5.3 3 2.8 1.7 5.3 3 11 6.3 5.2 3.1 11 6.3 5.3 3 2.8 1.7 5.3 3 2.8 1.7 5.3 3 15.4 8.9.7.4 21.1 12.2 7.8 4.5-6.3 3.7-9.8-5.7zm-8.2 15.5l-1-.6-4.3-2.5-4.1-2.4-4-2.3-2.8-1.6-4.9-2.8-3.2-1.8-6.1-3.5-4-2.3-37.6-21.8-2.8-1.6-7.1-4.1-27.5-15.9-5.8-3.4-4.7 2.7-2.1 1.2c-.2.1-.3.3-.2.5 0 .1.1.1.2.1l5.3 3 10.9 6.3 5.3 3 2.8 1.6 3.8 2.2 9.1 5.3 2.7 1.6 2.8 1.6 26 15 14.4 8.3.3.2 9.8 5.6 5.3 3 2.8 1.6 5.3 3 5.3 3.1 2.8 1.6c.3.2.8.1 1.2-.1l6-3.4.7-.4h-.6zm29.4-21.5l-16.6-9.6-1.6-.9-7.8-4.5-.7-.4-16.1-9.3-.6-.4-36.7-21.2-1.7-1-26.2-15.1-2.7-1.6c-.3-.2-.3-.4 0-.6l2.2-1.3c.3-.2.8-.2 1.1 0l5.3 3 16.2 9.4.7.4 3.2 1.9 2.3 1.3 6 3.4 5.3 3 3.2 1.9.9.5 7.5 4.3 5.3 3 3.2 1.9 5.3 3 3.1 1.8 5.3 3 3.2 1.9 1.8 1 6.5 3.7 5.3 3 3.2 1.9 5.3 3 3 1.8 5.3 3 3.2 1.9 5.3 3c.3.2.3.4 0 .6l-2.8 1.6-4.7-2.3zm-34 7.8l-2.1-1.2-10.6-6.1-.7-.4-2.9-1.6-5.2-3.1-2.8-1.6-5.3-3-10.9-6.4-2.6-1.4-13.6-7.9-3.1-1.8-33.7-19.4-7.6-4.4 6.3-3.7 5.7 3.3 10.7 6.1 3.6 2.2 11 6.3 5.3 3 11 6.4 5.2 3 13.7 7.9 2.5 1.5 19 10.9 5.3 3.1 2.9 1.6 5.2 3.1 2.8 1.6 11.9 6.8-6.4 3.7-9-5.2zm4 11.5l-48.3-27.9-.2-.1-52.5-30.4-12-6.9 6.3-3.6 10.2 5.8 2.7 1.6 3.1 1.8 2.9 1.6 1.4.9 16.2 9.3 3.9 2.3 10.9 6.3 1.5.8 14.8 8.6 1.9 1.1 16.4 9.4 3.1 1.8 24.2 14 2.6 1.5-6.4 3.6-2.7-1.5z" class="st75"/>
            <path d="M540.4 426.9l125.9 72.7c3.3 1.9 7.5.8 9.4-2.5l49.6-85.9c1.9-3.3.8-7.5-2.5-9.4L596.9 329c-3.3-1.9-7.5-.8-9.4 2.5l-49.6 85.9c-1.9 3.3-.8 7.6 2.5 9.5zm2.6-6.3l11.2-19.1 29.7-50.7 9.2-15.7h.1l14.5 8.4 29.4 17 2.7 1.6 5.3 3.1 63.9 37 1.2.7 9 5.2c.1 0 .1.1 0 .2l-6.4 11-43.7 74.5s0 .1-.1.1l-24.3-14.1-5.3-3.1-2.7-1.6-10.9-6.3-15.6-9-2.9-1.7-23.8-13.8-40.5-23.5v-.2z" class="st2"/>
            <g>
              <path fill="#fbfcff" d="M626.4 468.7l10.9 6.3 2.7 1.6 5.3 3.1 24.3 14c.1 0 .1 0 .1-.1l43.2-74.9-86.5 50z"/>
            </g>
            <g>
              <path d="M543 420.8l40.5 23.5 23.8 13.8 2.9 1.7 15.6 9 10.9 6.3 2.7 1.6 5.3 3.1 24.3 14.1c.1 0 .1 0 .1-.1l43.7-74.5 6.4-10.9v-.2l-9-5.2-1.2-.7-63.9-37-5.3-3.1-2.7-1.6-29.4-17-14.5-8.4h-.1l-9.2 15.7-29.7 50.7-11.2 19v.2z" class="st75"/>
            </g>
            <g>
              <path d="M553.4 422.8l110.4 63.7 42.9-74.2-27.8-16-82.6-47.7z" class="st76"/>
              <path d="M596.3 348.6l110.4 63.7 2.8-4.8c.4-.6.2-1.5-.5-1.8l-108.1-62.4c-.6-.4-1.5-.2-1.8.5l-2.8 4.8z" class="st76"/>
              <path d="M595.7 349.5l93.4 53.9L704 412l2.2 1.2 1.9-3.4L597.7 346z" class="st76"/>
              <linearGradient id="SVGID_79_" x1="465.183" x2="501.671" y1="411.027" y2="474.226" gradientTransform="matrix(-1 0 0 1 1150.298 0)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#287aff"/>
                <stop offset="1" stop-color="#287aff" stop-opacity="0"/>
              </linearGradient>
              <path fill="url(#SVGID_79_)" d="M630.4 467.2l33.4 19.3 42.4-73.3-33.5-19.3z"/>
              <path d="M604.5 354.5l14.9 8.6.1-2.6-12.8-7.4zm61.2 51.557l-76.987-44.45 7-12.124 76.987 44.45z" class="st75"/>
              <path d="M687.37 430.902l-20.524-11.85 9.25-16.02 20.524 11.85z" class="st0"/>
              <path d="M663.4 472.287l-20.524-11.85 23.9-41.394 20.524 11.85z" class="st75"/>
              <path d="M671 421.5c1.8-3.2 5.9-4.3 9.1-2.4 3.2 1.8 4.3 5.9 2.4 9.1l-11.5-6.7z" class="st76"/>
              <circle cx="682.6" cy="415.2" r="3.7" class="st76"/>
              <path d="M598.8 362.2l61.3 35.4c1.2.7 2.7.3 3.4-.9.7-1.2.3-2.7-.9-3.4l-61.3-35.4c-1.2-.7-2.7-.3-3.4.9-.7 1.2-.3 2.7.9 3.4z" class="st0"/>
              <g>
                <path d="M586.4 401l42.8 24.7c1 .6 2.4.2 3-.8l3.8-6.6c.6-1 .2-2.4-.8-3l-42.8-24.7c-1-.6-2.4-.2-3 .8l-3.8 6.6c-.6 1.1-.2 2.4.8 3z" class="st0"/>
                <circle cx="627.6" cy="417.9" r="2.5" class="st76"/>
                <circle cx="622" cy="414.7" r="2.5" class="st76"/>
                <circle cx="616.4" cy="411.4" r="2.5" class="st76"/>
                <circle cx="610.8" cy="408.2" r="2.5" class="st76"/>
                <circle cx="605.2" cy="405" r="2.5" class="st76"/>
                <circle cx="599.6" cy="401.7" r="2.5" class="st76"/>
                <circle cx="594" cy="398.5" r="2.5" class="st76"/>
              </g>
              <g>
                <path fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2.552" d="M667.5 424.5l13.5 7.8m-15.1-5.1l13.5 7.8m-15.1-5l13.5 7.8m-15.1-5l13.5 7.8m-15.1-5.1l13.5 7.8m-15.1-5l13.5 7.8m-15.1-5.1l13.5 7.8"/>
                <path fill="none" stroke="#143da1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2.552" d="M667.5 424.5l13.5 7.8m-15.1-5.1l8.7 5.1m-10.3-2.3l5.9 3.4m-7.5-.6l11.5 6.6m-13.1-3.9l8.8 5.1m-10.4-2.3l13.5 7.8m-15.1-5.1l10.3 6"/>
              </g>
              <g>
                <path d="M589.8 424.3l14.1 8.1c1.2.7 2.8.3 3.5-.9l3.1-5.3c.7-1.2.3-2.8-.9-3.5l-14.1-8.1c-1.2-.7-2.8-.3-3.5.9l-3.1 5.3c-.8 1.2-.4 2.8.9 3.5z" class="st75"/>
              </g>
            </g>
            <linearGradient id="SVGID_80_" x1="440.442" x2="669.161" y1="493.674" y2="493.674" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_80_)" d="M535.2 428.1c1.9 0 3.7.4 5 1.1l126.7 73.1c1.2.7 1.8 1.6 1.8 2.6 0 1.2-1 2.5-2.7 3.5l-85 49.1c-1.8 1-4.2 1.6-6.5 1.6-1.9 0-3.7-.4-5-1.1l-126.7-73.1c-1.2-.7-1.8-1.6-1.8-2.6 0-1.2 1-2.5 2.7-3.5l85-49.1c1.7-1 4.1-1.6 6.5-1.6m0-.5c-2.4 0-4.8.6-6.8 1.7l-85 49.1c-3.5 2-3.9 5.1-.9 6.9l126.7 73.1c1.4.8 3.3 1.2 5.2 1.2 2.4 0 4.8-.6 6.8-1.7l85-49.1c3.5-2 3.9-5.1.9-6.9l-126.7-73.1c-1.4-.8-3.3-1.2-5.2-1.2z"/>
            <linearGradient id="SVGID_81_" x1="530.654" x2="594.869" y1="358.976" y2="394.148" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_81_)" d="M593.5 328.5c1.1 0 2.2.3 3.2.9l125.9 72.7c1.5.9 2.6 2.2 3 3.9.4 1.7.2 3.4-.6 4.9l-49.6 85.9c-1.2 2-3.3 3.2-5.6 3.2-1.1 0-2.2-.3-3.2-.9l-125.9-72.7c-3.1-1.8-4.1-5.7-2.4-8.8l49.6-85.9c1.1-1.9 3.3-3.2 5.6-3.2m0-.4c-2.4 0-4.7 1.2-6 3.5l-49.6 85.9c-1.9 3.3-.8 7.5 2.5 9.4l125.9 72.7c1.1.6 2.3.9 3.4.9 2.4 0 4.7-1.2 6-3.5l49.6-85.9c1.9-3.3.8-7.5-2.5-9.4L596.9 329c-1.1-.6-2.3-.9-3.4-.9z"/>
            <g>
              <path d="M604.1 427.8v12.6l3.8-2.2 4.5-.9z" class="st0"/>
              <path d="M605.515 434.558l1.872-.706 3.28 8.702-1.872.706z" class="st0"/>
            </g>
          </g>
          <g>
            <radialGradient id="SVGID_82_" cx="887.028" cy="546.956" r="34.688" gradientTransform="matrix(1 -.1763 0 1 0 62.337)" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </radialGradient>
            <path fill="url(#SVGID_82_)" d="M934.6 461v.5c-.5 10.6-15.2 21.6-33.4 24.8-18.2 3.2-32.9-2.6-33.4-13v-1c.3-18.3 15.1-35.6 33.4-38.8 18.3-3.2 33.1 8.8 33.4 27v.5z"/>
            <linearGradient id="SVGID_83_" x1="871.021" x2="894.353" y1="438.421" y2="457.014" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_83_)" d="M907.1 432.9v.4c15.5 0 27 11.4 27.2 27.2v1c-.2 5-3.7 10.3-9.9 14.9s-14.5 8-23.2 9.6c-3.3.6-6.5.9-9.6.9-13.6 0-23-5.4-23.3-13.5v-1c.1-8.8 3.6-17.6 9.9-24.8 6.2-7.2 14.5-12.1 23.2-13.6 1.9-.3 3.9-.5 5.8-.5l-.1-.6m0 0c-1.9 0-3.9.2-5.8.5-18.3 3.2-33.1 20.5-33.4 38.8v1c.4 8.5 10.2 13.9 23.7 13.9 3.1 0 6.3-.3 9.7-.9 18.2-3.2 32.9-14.2 33.4-24.8v-1c-.3-16.1-12.1-27.5-27.6-27.5z"/>
            <radialGradient id="SVGID_84_" cx="886.922" cy="419.138" r="20.706" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".036" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </radialGradient>
            <circle cx="899.5" cy="422.2" r="20.7" fill="url(#SVGID_84_)"/>
            <linearGradient id="SVGID_85_" x1="864.404" x2="897.775" y1="420.596" y2="422.147" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_85_)" d="M899.5 401.9c11.2 0 20.3 9.1 20.3 20.3s-9.1 20.3-20.3 20.3-20.3-9.1-20.3-20.3 9.1-20.3 20.3-20.3m0-.4c-11.4 0-20.7 9.3-20.7 20.7s9.3 20.7 20.7 20.7 20.7-9.3 20.7-20.7-9.3-20.7-20.7-20.7z"/>
          </g>
          <g>
            <path d="M941.5 487.1c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.3c-.2.1-.5.3-.7.4-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3z" class="st76"/>
            <linearGradient id="SVGID_86_" x1="965.961" x2="958.923" y1="415.526" y2="427.716" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_86_)" d="M941.5 487.1c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.3c-.2.1-.5.3-.7.4-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3z"/>
            <linearGradient id="SVGID_87_" x1="932.787" x2="913.488" y1="472.986" y2="506.412" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#143da1" stop-opacity="0"/>
              <stop offset=".262" stop-color="#143da1"/>
              <stop offset=".66" stop-color="#143da1"/>
              <stop offset="1" stop-color="#143da1" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_87_)" d="M941.5 487.1c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.3c-.2.1-.5.3-.7.4-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3z"/>
            <linearGradient id="SVGID_88_" x1="943.347" x2="919.63" y1="454.694" y2="495.774" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#287aff" stop-opacity="0"/>
              <stop offset=".348" stop-color="#287aff"/>
              <stop offset="1" stop-color="#287aff" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_88_)" d="M941.5 487.1c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.3c-.2.1-.5.3-.7.4-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3z" opacity=".7"/>
            <linearGradient id="SVGID_89_" x1="955.614" x2="943.286" y1="433.447" y2="454.8" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#287aff" stop-opacity="0"/>
              <stop offset=".348" stop-color="#287aff"/>
              <stop offset=".626" stop-color="#287aff"/>
              <stop offset="1" stop-color="#287aff" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_89_)" d="M941.5 487.1c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.3c-.2.1-.5.3-.7.4-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3z"/>
            <linearGradient id="SVGID_90_" x1="919.195" x2="951.572" y1="431.011" y2="449.704" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_90_)" d="M966.7 415.7l7.5 4.1V436c0 14.4-8.7 34.6-23.4 54-3.4-.6-6.5-1.7-9.2-3.2l-7.9-4.3c-7.9-4.5-12.3-13.2-12.3-24.4V442l.6-.3c6.1-3.8 12.1-9.3 17.3-15.9 1.7-2.1 3.2-4.4 4.7-6.7 1.5.6 3 1 4.7 1.2.9.1 1.7.2 2.6.2 4.6 0 9.7-1.5 14.8-4.2h.1c.2-.4.4-.5.5-.6m0-.4c-.2.1-.5.3-.7.4-5.1 2.7-10.1 4.2-14.6 4.2-.9 0-1.7-.1-2.6-.2-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4V458c0 11.2 4.2 20 12.4 24.8l7.9 4.3c2.7 1.6 5.9 2.7 9.6 3.3 14.5-19.2 23.6-39.4 23.6-54.4v-16.3l-7.9-4.4z"/>
            <linearGradient id="SVGID_91_" x1="922.615" x2="1036.806" y1="454.994" y2="454.994" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_91_)" d="M973.9 420c-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4v16.3c0 15 7.5 25.6 22 28 14.5-19.2 23.6-39.4 23.6-54.4v-16.3c-.2.3-.5.4-.7.5z"/>
            <path d="M973.9 420c-6.1 3.3-12 4.7-17.2 4-1.7-.2-3.4-.7-4.9-1.3-1.5 2.4-3.1 4.7-4.9 6.9-5.2 6.6-11.1 12.1-17.2 15.9-.2.2-.5.3-.7.4v16.3c0 15 7.5 25.6 22 28 14.5-19.2 23.6-39.4 23.6-54.4v-16.3c-.2.3-.5.4-.7.5z" class="st76"/>
            <g>
              <path d="M949.3 485.7c-10.4-2.7-16-10.9-16-23.4v-14c6.1-4 11.9-9.5 17-16.1l3.3-4.5c.9.2 1.7.4 2.6.5 1 .1 2 .2 3.1.2 3.5 0 7.3-.7 11.1-2.2v9.8c0 13.1-7.9 31.6-21.1 49.7z" class="st75"/>
            </g>
          </g>
          <g>
            <g class="st111">
              <defs>
                <path id="SVGID_92_" d="M944.8 466.1l-.1-17.3 15.1-8.7.1 17.3z" class="st111"/>
              </defs>
              <clipPath id="SVGID_93_">
                <use overflow="visible" xlink:href="#SVGID_92_"/>
              </clipPath>
              <g clip-path="url(#SVGID_93_)">
                <path d="M950.5 456.2l5.9-10.2 1.7 1-7.6 13.2-4-2.3 1.7-3 2.3 1.3m1.8-11.8c-4.2 2.4-7.5 8.2-7.5 13 0 4.8 3.4 6.7 7.6 4.3 4.2-2.4 7.5-8.2 7.5-13 0-4.8-3.4-6.7-7.6-4.3" class="st0"/>
                <path d="M956.4 445.9l-5.9 10.3-2.3-1.3-1.7 3 4 2.3 7.6-13.3-1.7-1" class="st76"/>
              </g>
            </g>
          </g>
          <g id="Letter_5_1_">
            <linearGradient id="SVGID_94_" x1="797.34" x2="841.272" y1="396.24" y2="396.24" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#287aff"/>
              <stop offset=".61" stop-color="#143da1"/>
              <stop offset="1" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_94_)" d="M840 399.7l-12.5 7.2c-1.5.9-3.7.9-5 .2L798.2 393c-.6-.4-.9-.8-.9-1.3v-6.9l43.9 6.4v6.7c.1.6-.3 1.3-1.2 1.8z"/>
            <linearGradient id="SVGID_95_" x1="839.204" x2="807.703" y1="385.861" y2="389.361" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_95_)" d="M822.5 400.3l-24.3-14.1c-1.3-.7-1.1-2 .4-2.9l12.5-7.2c1.5-.9 3.7-.9 5-.2l24.3 14.1c1.3.7 1.1 2-.4 2.9l-12.5 7.2c-1.4.8-3.7.9-5 .2z"/>
            <linearGradient id="SVGID_96_" x1="832.715" x2="789.991" y1="386.582" y2="391.329" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_96_)" d="M813.9 375.4v.4c.8 0 1.5.2 2 .4l24.3 14.1c.3.2.7.5.7.9s-.4.9-1 1.3l-12.5 7.2c-.7.4-1.7.7-2.6.7-.8 0-1.5-.2-2-.4l-24.3-14.1c-.3-.2-.7-.5-.7-.9s.4-.9 1-1.3l12.5-7.2c.7-.4 1.7-.7 2.6-.7v-.4m0 0c-1 0-2 .2-2.8.7l-12.5 7.2c-1.5.9-1.6 2.1-.4 2.9l24.3 14.1c.6.3 1.4.5 2.2.5 1 0 2-.2 2.8-.7l12.5-7.2c1.5-.9 1.6-2.1.4-2.9l-24.3-14.1c-.6-.3-1.4-.5-2.2-.5z"/>
            <g>
              <path d="M805.3 393.7c0-.6-.4-1.3-.9-1.5s-.9-.1-.9.5.4 1.3.9 1.5.9 0 .9-.5zm-2.3-1.3c0-.6-.4-1.3-.9-1.5s-.9-.1-.9.5.4 1.3.9 1.5.9 0 .9-.5zm-2.3-1.4c0-.6-.4-1.3-.9-1.5-.5-.3-.9-.1-.9.5s.4 1.3.9 1.5.9.1.9-.5z" class="st0"/>
            </g>
            <g class="st17">
              <path d="M814.1 376.8l21.9 12.7v1.4l-12.4 7.1-21.9-12.7v-1.4z" class="st2 shadow4"/>
            </g>
            <g>
              <g class="st111">
                <defs>
                  <path id="SVGID_98_" d="M835.7 404.1l4-2.4 8.6 5-4 2.3z" class="st111"/>
                </defs>
                <clipPath id="SVGID_99_">
                  <use overflow="visible" xlink:href="#SVGID_98_"/>
                </clipPath>
                <g clip-path="url(#SVGID_99_)">
                  <path d="M842.9 407.3l-6-3.4c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l6 3.4c.2.1.6.1.8 0 .2-.2.2-.4 0-.5m3.4.1l-7.8-4.5c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l7.8 4.5c.2.1.6.1.8 0 .2-.2.2-.4 0-.5m-.2-2l-6-3.4c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l6 3.4c.2.1.6.1.8 0 .2-.1.2-.4 0-.5" class="st152"/>
                </g>
              </g>
            </g>
            <g>
              <path d="M814 372.6l22.4 13v1.4l-12.6 7.3-22.5-13v-1.4z" class="st76"/>
              <linearGradient id="SVGID_100_" x1="840.547" x2="800.942" y1="386.714" y2="386.714" gradientTransform="translate(-1.135 1.62)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_100_)" d="M823.8 392.9l12.6-7.3v1.4l-12.6 7.3-22.5-13v-1.4z"/>
              <g class="st111">
                <path d="M801.7 379.7l14.2 2.2-2.3-9.1 2.3 9.1-14.2-2.2" class="st0"/>
                <path d="M820.3 384.5l15.7 1.3-11.9 6.9-3.8-8.2" class="st75"/>
                <path d="M819 384.4l3.6 7.8-3.6-7.8" class="st0"/>
                <path d="M801.7 379.7l11.9-6.9 2.3 9.1-14.2-2.2" class="st75"/>
                <path d="M819 384.4l3.6 7.8-3.6-7.8" class="st0"/>
                <path d="M802.5 380.6l13.6 2.1.4 1.4 2.5.3 3.6 7.8-20.1-11.6m12.3-7.5l20.7 12-15.6-1.4h-.6l-.6-.1-1.2-.1-.2-.6-.1-.4-.1-.4-2.3-9" class="st75"/>
              </g>
            </g>
          </g>
          <g id="Letter_4_1_">
            <linearGradient id="SVGID_101_" x1="1059.751" x2="1143.862" y1="627.177" y2="627.177" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#287aff"/>
              <stop offset=".61" stop-color="#143da1"/>
              <stop offset="1" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_101_)" d="M1141.5 633.8l-23.9 13.8c-2.8 1.6-7.1 1.8-9.6.4l-46.6-26.9c-1.2-.7-1.7-1.6-1.6-2.6v-13.1l84.1 12.3v12.7c.1 1.2-.7 2.4-2.4 3.4z"/>
            <linearGradient id="SVGID_102_" x1="1139.902" x2="1079.591" y1="607.305" y2="614.007" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_102_)" d="M1108 634.9l-46.6-26.9c-2.5-1.4-2.1-3.9.7-5.5l23.9-13.8c2.8-1.6 7.1-1.8 9.6-.4l46.6 26.9c2.5 1.4 2.1 3.9-.7 5.5l-23.9 13.8c-2.8 1.6-7.1 1.8-9.6.4z"/>
            <linearGradient id="SVGID_103_" x1="1127.479" x2="1045.681" y1="608.686" y2="617.774" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_103_)" d="M1091.4 587.3v.8c1.5 0 2.8.3 3.8.8l46.6 26.9c.6.3 1.3.9 1.3 1.7 0 .8-.7 1.7-1.9 2.4l-23.9 13.8c-1.4.8-3.2 1.2-5 1.2-1.5 0-2.8-.3-3.8-.8l-46.6-26.9c-.6-.3-1.3-.9-1.3-1.7 0-.8.7-1.7 1.9-2.4l23.9-13.8c1.4-.8 3.2-1.2 5-1.2v-.8m0 0c-1.9 0-3.9.5-5.4 1.4l-23.9 13.8c-2.8 1.6-3.1 4.1-.7 5.5l46.6 26.9c1.1.6 2.6 1 4.2 1 1.9 0 3.9-.5 5.4-1.4l23.9-13.8c2.8-1.6 3.1-4.1.7-5.5l-46.6-26.9c-1.1-.7-2.6-1-4.2-1z"/>
            <g>
              <path d="M1075 622.3c0-1.1-.8-2.4-1.7-3-.9-.5-1.7-.1-1.7 1s.8 2.4 1.7 3c.9.5 1.7.1 1.7-1zm-4.4-2.6c0-1.1-.8-2.4-1.7-3-.9-.5-1.7-.1-1.7 1s.8 2.4 1.7 3c.9.6 1.7.1 1.7-1zm-4.4-2.5c0-1.1-.8-2.4-1.7-3-.9-.5-1.7-.1-1.7 1s.8 2.4 1.7 3c1 .5 1.7.1 1.7-1z" class="st0"/>
            </g>
            <g class="st17">
              <path d="M1091.9 590l41.9 24.3v2.6l-23.7 13.7-41.9-24.3v-2.7z" class="st2 shadow4"/>
            </g>
            <g>
              <g class="st111">
                <defs>
                  <path id="SVGID_105_" d="M1133.2 642.1l7.6-4.4 16.4 9.5-7.6 4.4z" class="st111"/>
                </defs>
                <clipPath id="SVGID_106_">
                  <use overflow="visible" xlink:href="#SVGID_105_"/>
                </clipPath>
                <g clip-path="url(#SVGID_106_)">
                  <path d="M1147 648.3l-11.4-6.6c-.4-.2-1.1-.2-1.5 0-.4.2-.4.6 0 .9l11.4 6.6c.4.2 1.1.2 1.5 0 .4-.3.4-.7 0-.9m6.4.2l-14.8-8.6c-.4-.2-1.1-.2-1.5 0-.4.2-.4.6 0 .9l14.8 8.6c.4.2 1.1.2 1.5 0 .4-.3.4-.7 0-.9m-.4-3.7l-11.4-6.6c-.4-.2-1.1-.2-1.5 0s-.4.6 0 .9l11.4 6.6c.4.2 1.1.2 1.5 0 .5-.3.4-.7 0-.9" class="st152"/>
                </g>
              </g>
            </g>
            <g>
              <path d="M1091.6 581.9l42.9 24.8v2.7l-24.2 14.1-42.9-24.9v-2.7z" class="st76"/>
              <linearGradient id="SVGID_107_" x1="1141.632" x2="1065.805" y1="609.716" y2="609.716" gradientTransform="translate(-1.135 1.62)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_107_)" d="M1110.3 620.8l24.2-14.1v2.7l-24.2 14.1-42.9-24.9v-2.7z"/>
              <g class="st111">
                <path d="M1068 595.6l27.3 4.2-4.5-17.5 4.5 17.5-27.3-4.2" class="st0"/>
                <path d="M1103.7 604.6l30 2.6-22.8 13.2-7.2-15.8" class="st75"/>
                <path d="M1101.2 604.4l6.9 15.1-6.9-15.1" class="st0"/>
                <path d="M1068 595.6l22.8-13.3 4.5 17.5-27.3-4.2" class="st75"/>
                <path d="M1101.2 604.4l6.9 15.1-6.9-15.1" class="st0"/>
                <path d="M1069.6 597.2l26 4 .7 2.8 4.9.4 6.9 15.1-38.5-22.3" class="st75"/>
                <path d="M1093.2 582.8l39.7 23-29.9-2.6-1.2-.1-1.2-.1-2.3-.2-.3-1.3-.2-.7-.2-.7-4.4-17.3" class="st75"/>
              </g>
            </g>
          </g>
          <g id="Letter_3_1_">
            <linearGradient id="SVGID_108_" x1="1042.522" x2="1093.67" y1="528.461" y2="528.461" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#287aff"/>
              <stop offset=".61" stop-color="#143da1"/>
              <stop offset="1" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_108_)" d="M1092.2 532.5l-14.6 8.4c-1.7 1-4.3 1.1-5.8.2l-28.3-16.4c-.7-.4-1-1-1-1.6v-8l51.1 7.5v7.8c.1.7-.3 1.5-1.4 2.1z"/>
            <linearGradient id="SVGID_109_" x1="1091.263" x2="1054.587" y1="516.377" y2="520.452" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_109_)" d="M1071.9 533.1l-28.3-16.4c-1.5-.9-1.3-2.4.4-3.4l14.6-8.4c1.7-1 4.3-1.1 5.8-.2l28.3 16.4c1.5.9 1.3 2.4-.4 3.4l-14.6 8.4c-1.7 1-4.3 1.1-5.8.2z"/>
            <linearGradient id="SVGID_110_" x1="1083.708" x2="1033.966" y1="517.217" y2="522.743" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_110_)" d="M1061.8 504.2v.5c.9 0 1.7.2 2.3.5l28.3 16.4c.3.2.8.5.8 1s-.4 1.1-1.2 1.5l-14.6 8.4c-.8.5-1.9.8-3.1.8-.9 0-1.7-.2-2.3-.5l-28.3-16.4c-.3-.2-.8-.5-.8-1s.4-1.1 1.2-1.5l14.6-8.4c.8-.5 1.9-.8 3.1-.8v-.5m0 0c-1.1 0-2.4.3-3.3.8l-14.6 8.4c-1.7 1-1.9 2.5-.4 3.4l28.3 16.4c.7.4 1.6.6 2.5.6 1.1 0 2.3-.3 3.3-.8l14.6-8.4c1.7-1 1.9-2.5.4-3.4l-28.3-16.4c-.7-.4-1.6-.6-2.5-.6z"/>
            <g>
              <path d="M1051.8 525.5c0-.7-.5-1.5-1-1.8-.6-.3-1-.1-1 .6s.5 1.5 1 1.8c.5.3 1 0 1-.6zm-2.7-1.6c0-.7-.5-1.5-1-1.8-.6-.3-1-.1-1 .6s.5 1.5 1 1.8c.6.4 1 .1 1-.6zm-2.6-1.5c0-.7-.5-1.5-1-1.8-.6-.3-1-.1-1 .6s.5 1.5 1 1.8c.5.3 1 .1 1-.6z" class="st0"/>
            </g>
            <g class="st17">
              <path d="M1062.1 505.8l25.4 14.8v1.6l-14.4 8.4-25.4-14.9v-1.6z" class="st2 shadow4"/>
            </g>
            <g>
              <g class="st111">
                <defs>
                  <path id="SVGID_112_" d="M1087.2 537.6l4.6-2.7 10 5.7-4.6 2.7z" class="st111"/>
                </defs>
                <clipPath id="SVGID_113_">
                  <use overflow="visible" xlink:href="#SVGID_112_"/>
                </clipPath>
                <g clip-path="url(#SVGID_113_)">
                  <path d="M1095.5 541.3l-6.9-4c-.3-.1-.7-.1-.9 0-.3.1-.3.4 0 .5l6.9 4c.3.1.7.1.9 0 .3-.1.3-.4 0-.5m4 .1l-9-5.2c-.3-.1-.7-.2-.9 0-.3.1-.3.4 0 .5l9 5.2c.3.1.7.1.9 0 .2-.1.2-.3 0-.5m-.3-2.3l-6.9-4c-.3-.1-.7-.1-.9 0-.3.2-.3.4 0 .5l6.9 4c.3.1.7.2.9 0 .3-.1.3-.3 0-.5" class="st152"/>
                </g>
              </g>
            </g>
            <g>
              <path d="M1061.9 500.9L1088 516v1.7l-14.7 8.5-26.1-15.1v-1.6z" class="st76"/>
              <linearGradient id="SVGID_114_" x1="1092.538" x2="1046.428" y1="517.808" y2="517.808" gradientTransform="translate(-1.135 1.62)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_114_)" d="M1073.3 524.6l14.7-8.6v1.7l-14.7 8.5-26.1-15.1v-1.6z"/>
              <g class="st111">
                <path d="M1047.5 509.2l16.6 2.6-2.7-10.6 2.7 10.6-16.6-2.6" class="st0"/>
                <path d="M1069.2 514.7l18.3 1.6-13.9 8.1-4.4-9.7" class="st75"/>
                <path d="M1067.7 514.6l4.2 9.2-4.2-9.2" class="st0"/>
                <path d="M1047.5 509.2l13.9-8 2.7 10.6-16.6-2.6" class="st75"/>
                <path d="M1067.7 514.6l4.2 9.2-4.2-9.2" class="st0"/>
                <path d="M1048.5 510.2l15.8 2.4.5 1.8 2.9.2 4.2 9.2-23.4-13.6" class="st75"/>
                <path d="M1062.9 501.5l24.1 14-18.2-1.6-.7-.1h-.7l-1.4-.1-.2-.8-.1-.5-.1-.4-2.7-10.5" class="st75"/>
              </g>
            </g>
          </g>
          <g id="Letter_2_1_">
            <linearGradient id="SVGID_115_" x1="965.189" x2="1009.122" y1="503.155" y2="503.155" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#287aff"/>
              <stop offset=".61" stop-color="#143da1"/>
              <stop offset="1" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_115_)" d="M1007.9 506.6l-12.5 7.2c-1.5.9-3.7.9-5 .2l-24.3-14c-.6-.4-.9-.8-.9-1.3v-6.9l43.9 6.4v6.7c.1.6-.3 1.2-1.2 1.7z"/>
            <linearGradient id="SVGID_116_" x1="1007.053" x2="975.552" y1="492.776" y2="496.276" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_116_)" d="M990.4 507.2l-24.3-14.1c-1.3-.7-1.1-2 .4-2.9L979 483c1.5-.9 3.7-.9 5-.2l24.3 14.1c1.3.7 1.1 2-.4 2.9l-12.5 7.2c-1.5.8-3.7.9-5 .2z"/>
            <linearGradient id="SVGID_117_" x1="1000.565" x2="957.84" y1="493.497" y2="498.244" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_117_)" d="M981.7 482.3v.4c.8 0 1.5.2 2 .4l24.3 14.1c.3.2.7.5.7.9s-.4.9-1 1.3l-12.5 7.2c-.7.4-1.7.7-2.6.7-.8 0-1.5-.2-2-.4l-24.3-14.1c-.3-.2-.7-.5-.7-.9s.4-.9 1-1.3l12.5-7.2c.7-.4 1.7-.7 2.6-.7v-.4m0 0c-1 0-2 .2-2.8.7l-12.5 7.2c-1.5.9-1.6 2.1-.4 2.9l24.3 14.1c.6.3 1.4.5 2.2.5 1 0 2-.2 2.8-.7l12.5-7.2c1.5-.9 1.6-2.1.4-2.9l-24.3-14.1c-.6-.3-1.3-.5-2.2-.5z"/>
            <g>
              <path d="M973.1 500.6c0-.6-.4-1.3-.9-1.5-.5-.3-.9-.1-.9.5s.4 1.3.9 1.5c.6.3.9.1.9-.5zm-2.2-1.3c0-.6-.4-1.3-.9-1.5-.5-.3-.9-.1-.9.5s.4 1.3.9 1.5c.5.3.9 0 .9-.5zm-2.3-1.4c0-.6-.4-1.3-.9-1.5-.5-.3-.9-.1-.9.5s.4 1.3.9 1.5c.5.3.9.1.9-.5z" class="st0"/>
            </g>
            <g class="st17">
              <path d="M982 483.7l21.8 12.7v1.4l-12.3 7.2-21.9-12.8v-1.3z" class="st2 shadow4"/>
            </g>
            <g>
              <g class="st111">
                <defs>
                  <path id="SVGID_119_" d="M1003.6 511l4-2.3 8.5 4.9-4 2.3z" class="st111"/>
                </defs>
                <clipPath id="SVGID_120_">
                  <use overflow="visible" xlink:href="#SVGID_119_"/>
                </clipPath>
                <g clip-path="url(#SVGID_120_)">
                  <path d="M1010.7 514.2l-6-3.4c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l6 3.4c.2.1.6.1.8 0 .3-.2.3-.4 0-.5m3.4.1l-7.8-4.5c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l7.8 4.5c.2.1.6.1.8 0 .2-.2.2-.4 0-.5m-.2-2l-6-3.4c-.2-.1-.6-.1-.8 0-.2.1-.2.3 0 .5l6 3.4c.2.1.6.1.8 0s.2-.3 0-.5" class="st152"/>
                </g>
              </g>
            </g>
            <g>
              <path d="M981.8 479.5l22.4 13v1.4l-12.6 7.3-22.4-13v-1.4z" class="st76"/>
              <linearGradient id="SVGID_121_" x1="1008.235" x2="968.63" y1="493.881" y2="493.881" gradientTransform="translate(-1.135 1.62)" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_121_)" d="M991.6 499.8l12.6-7.3v1.4l-12.6 7.3-22.4-13v-1.4z"/>
              <g class="st111">
                <path d="M969.5 486.6l14.2 2.2-2.3-9.1 2.3 9.1-14.2-2.2" class="st0"/>
                <path d="M988.1 491.4l15.7 1.3-11.9 6.9-3.8-8.2" class="st75"/>
                <path d="M986.8 491.3l3.7 7.8-3.7-7.8" class="st0"/>
                <path d="M969.5 486.6l11.9-6.9 2.3 9.1-14.2-2.2" class="st75"/>
                <path d="M986.8 491.3l3.7 7.8-3.7-7.8" class="st0"/>
                <path d="M970.3 487.5l13.6 2.1.4 1.5 2.5.2 3.7 7.8-20.2-11.6m12.4-7.5l20.7 12-15.6-1.3-.7-.1-.6-.1-1.2-.1-.1-.6-.1-.4-.1-.4-2.3-9" class="st75"/>
              </g>
            </g>
          </g>
          <g id="Letter_1_1_">
            <linearGradient id="SVGID_122_" x1="1099.975" x2="1059.268" y1="447.864" y2="446.982" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#143da1"/>
              <stop offset=".39" stop-color="#143da1"/>
              <stop offset=".609" stop-color="#287aff"/>
            </linearGradient>
            <path fill="url(#SVGID_122_)" d="M1082.2 443.4v6.4c0 .8-.7 1.5-1.8 1.7l-32.2 3.8c-.7.1-1.5-.1-2.1-.4-.5-.3-.9-.7-1-1.2l-3.2-8.7c-.1-.2-.1-.4-.1-.5V438l40.4 5.4z"/>
            <g class="st111">
              <linearGradient id="SVGID_123_" x1="1083.816" x2="1035.082" y1="441.181" y2="440.125" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".464" stop-color="#143da1"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_123_)" d="M1077.9 433c-.6-.3-1.3-.5-2.1-.4l-32.2 3.8c-1.3.2-2.1 1.1-1.7 2.2l3.2 8.8c.2.5.6.9 1.1 1.2.6.3 1.3.5 2.1.4l32.2-3.8c1.3-.2 2.1-1.1 1.7-2.2l-3.2-8.8c-.2-.6-.6-1-1.1-1.2"/>
            </g>
            <g class="st32">
              <path d="M1077.8 442.3l-.2-.4-.2-.7-2.7-7.9-1.2.1-26.3 3.1-1 .2v1.2l.2.7 2.8 7.9.3.6 2.7-.3 21.9-2.6 3.7-.4v-1.3z" class="st2 shadow4"/>
            </g>
            <linearGradient id="SVGID_125_" x1="1040.573" x2="1073.032" y1="443.314" y2="439.364" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_125_)" d="M1076.2 432.9c.5 0 1 .1 1.5.4.4.2.7.6.9 1l3.2 8.8c.1.3.1.6-.1.9-.2.4-.7.7-1.3.7l-32.2 3.8h-.3c-.5 0-1-.1-1.5-.4-.4-.2-.7-.6-.9-1l-3.2-8.8c-.1-.3-.1-.6.1-.9.2-.4.7-.7 1.3-.7l32.2-3.8h.3m0-.4h-.4l-32.2 3.8c-1.3.2-2.1 1.1-1.7 2.2l3.2 8.8c.2.5.6.9 1.1 1.2.5.3 1.1.4 1.7.4h.4l32.2-3.8c1.3-.2 2.1-1.1 1.7-2.2l-3.2-8.8c-.2-.5-.6-.9-1-1.2-.6-.2-1.2-.4-1.8-.4z"/>
            <g class="st111">
              <defs>
                <path id="SVGID_126_" d="M1028.3 447.7l7.6-4.4 6.3 3.6-7.6 4.4z" class="st111"/>
              </defs>
              <clipPath id="SVGID_127_">
                <use overflow="visible" xlink:href="#SVGID_126_"/>
              </clipPath>
              <g clip-path="url(#SVGID_127_)">
                <path d="M1039.4 445.3c-.1-.1-.3-.1-.5-.1l-7.8.9c-.3 0-.5.2-.4.4 0 .1.1.1.1.2.1.1.3.1.5.1l7.8-.9c.3 0 .5-.2.4-.4 0-.1-.1-.2-.1-.2m.4 1.2c-.1-.1-.3-.1-.5-.1l-10.1 1.1c-.3 0-.5.2-.4.4 0 .1.1.1.1.2.1.1.3.1.5.1l10.1-1.1c.3 0 .5-.2.4-.4 0-.1 0-.2-.1-.2m.4 1.2c-.1-.1-.3-.1-.5-.1l-7.8.9c-.3 0-.5.2-.4.4 0 .1.1.1.1.2.1.1.3.1.5.1l7.8-.9c.3 0 .5-.2.4-.4 0-.1 0-.2-.1-.2" class="st152"/>
              </g>
            </g>
            <path d="M1074.7 432.2l-1.2.1-.2.1.1-.1z" class="st76"/>
            <linearGradient id="SVGID_128_" x1="1064.332" x2="1057.841" y1="445.675" y2="424.003" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".232" stop-color="#113493"/>
              <stop offset=".279" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_128_)" d="M1077.8 441.4l-3.7.5-21.9 2.6-2.7.3-.3-.7-2.8-7.9-.2-.6v-1.2l27.2-2.1-.1.1.2-.1 1.2-.1 3.1 8z"/>
            <path d="M1074.7 432.2l-1.2.1-.2.1.1-.1z" class="st76"/>
            <g>
              <path d="M1059.4 438.2l-13.1-3.6 3.1 8.7 10-5.1m15.2-6.9l-9.6 6.2 12.8 2.5-3.2-8.7m1.8 9.1l-12.2-2.4-1.6 1-2.1-.5-9.6 4.9 25.5-3" class="st75"/>
              <path d="M1073.5 431.1l-26.3 3.1 13.1 3.5.5.2.6.1 1 .3.7-.5.4-.2.4-.3 9.6-6.2" class="st75"/>
            </g>
            <g>
              <path d="M1074.7 431l-1.2.1-.2.1.1-.1z" class="st76"/>
              <linearGradient id="SVGID_129_" x1="1085.136" x2="1041.008" y1="433.977" y2="440.261" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#00074d"/>
                <stop offset=".994" stop-color="#143da1"/>
              </linearGradient>
              <path fill="url(#SVGID_129_)" d="M1077.8 440.2l-3.7.4-21.9 2.6-2.7.4-.3-.7-2.8-7.9-.2-.7 1.9-.2 25.3-3-.1.1.2-.1 1-.1 2.9 7.9z"/>
            </g>
            <g>
              <path d="M1059.4 438.2l-13.1-3.6 3.1 8.7 10-5.1m15.2-6.9l-9.6 6.2 12.8 2.5-3.2-8.7m1.8 9.1l-12.2-2.4-1.6 1-2.1-.5-9.6 4.9 25.5-3" class="st76"/>
              <path d="M1073.5 431.1l-26.3 3.1 13.1 3.5.5.2.6.1 1 .3.7-.5.4-.2.4-.3 9.6-6.2" class="st76"/>
            </g>
            <g>
              <g class="st111">
                <defs>
                  <path id="SVGID_130_" d="M1071.6 450v-1.9l6.6-.8-.1 1.9z" class="st111"/>
                </defs>
                <clipPath id="SVGID_131_">
                  <use overflow="visible" xlink:href="#SVGID_130_"/>
                </clipPath>
                <g clip-path="url(#SVGID_131_)">
                  <path d="M1077.3 447.5c-.5.1-.9.5-1 1.1 0 .5.4.9.9.8.5-.1.9-.5 1-1.1 0-.5-.4-.9-.9-.8m-2.4.2c-.5.1-.9.5-1 1.1 0 .5.4.9.9.8s.9-.5 1-1.1c0-.4-.4-.8-.9-.8m-2.3.3c-.5.1-.9.5-1 1.1 0 .5.4.9.9.8.5-.1.9-.5 1-1.1 0-.5-.4-.8-.9-.8" class="st2"/>
                </g>
              </g>
            </g>
          </g>
          <g>
            <linearGradient id="SVGID_132_" x1="398.986" x2="498.035" y1="437.065" y2="437.065" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_132_)" d="M495.8 431.7l-42.5 24.5c-2.7 1.5-6.7 1.7-9 .4l-43.8-25.3c-1.1-.6-1.6-1.5-1.5-2.4v-12.3h99v12c.1 1.1-.6 2.2-2.2 3.1z"/>
            <linearGradient id="SVGID_133_" x1="494.106" x2="421.922" y1="411.526" y2="419.547" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_133_)" d="M444.3 444.3L400.5 419c-2.3-1.3-2-3.7.6-5.2l42.5-24.5c2.7-1.5 6.7-1.7 9-.4l43.8 25.3c2.3 1.3 2 3.7-.6 5.2l-42.5 24.5c-2.6 1.6-6.7 1.7-9 .4z"/>
            <linearGradient id="SVGID_134_" x1="397.338" x2="470.772" y1="427.943" y2="411.655" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_134_)" d="M448.8 388.5c1.4 0 2.7.3 3.7.8l43.8 25.3c.9.5 1.3 1.1 1.3 1.8 0 .9-.7 1.8-2 2.5l-42.5 24.5c-1.3.8-3.1 1.2-4.9 1.2-1.4 0-2.7-.3-3.7-.8l-43.8-25.3c-.9-.5-1.3-1.1-1.3-1.8 0-.9.7-1.8 2-2.5l42.5-24.5c1.4-.8 3.1-1.2 4.9-1.2m0-.5c-1.8 0-3.6.4-5.1 1.3l-42.5 24.5c-2.7 1.5-3 3.9-.6 5.2l43.8 25.3c1 .6 2.4.9 3.9.9 1.8 0 3.6-.4 5.1-1.3l42.5-24.5c2.7-1.5 3-3.9.6-5.2l-43.8-25.3c-1.1-.6-2.5-.9-3.9-.9z"/>
          </g>
          <path fill="none" stroke="#71cbd5" stroke-miterlimit="10" stroke-width="1.375" d="M442.5 419.8c3.3 1.9 8.7 1.9 12 0 3.3-1.9 3.3-5 0-6.9-3.3-1.9-8.7-1.9-12 0-3.3 1.8-3.3 5 0 6.9z"/>
          <path fill="none" stroke="#71cbd5" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2.552" d="M448.5 389.4v26.3"/>
          <g>
            <linearGradient id="SVGID_135_" x1="430.189" x2="464.368" y1="349.939" y2="368.902" gradientUnits="userSpaceOnUse">
              <stop offset=".01" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_135_)" d="M465.2 317.9l-2 5-2.7 6.8-.3.8-6 15-.1.3-2 4.9-1.2 3.1-.4 1-1 2.6-.7 1.8-.9 2.4-12.7 31.7-3.3 8.3-.2.5-.4-.2-.3-.2-2.2-1.3c-.3-.1-.4-.3-.4-.7v-63.1c0-.7.5-1.5 1-1.8l32.3-18.7c.2-.1.4-.2.6-.1h.1l.1.1 2.3 1.3.3.2.1.3z"/>
            <path d="M461.9 348.2l-21.5 12.4c-.6.3-1 0-1-.8v-16.6c0-.8.5-1.6 1-2l21.5-12.4c.6-.3 1 0 1 .8v16.6c0 .8-.5 1.7-1 2z" class="st2"/>
            <path d="M457.9 350.5c0-4.2-3-5.9-6.6-3.8-3.7 2.1-6.6 7.2-6.6 11.5l13.2-7.7zm-10.5-6.9c0-2.3 1.6-5.2 3.7-6.4s3.7-.2 3.7 2.1-1.6 5.2-3.7 6.4c-2 1.2-3.7.3-3.7-2.1z" class="st2"/>
            <linearGradient id="SVGID_136_" x1="430.987" x2="465.278" y1="337.554" y2="381.946" gradientUnits="userSpaceOnUse">
              <stop offset=".006" stop-color="#143da1"/>
              <stop offset=".536" stop-color="#143da1"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_136_)" d="M465.6 318.6v63.1c0 .7-.5 1.5-1.1 1.8l-32.4 18.7c-.2.1-.3.1-.5.1-.1 0-.2 0-.3-.1-.1-.1-.2-.2-.2-.3v-63.4c0-.7.5-1.5 1-1.8l32.4-18.7c.2-.1.3-.1.5-.1h.2c.1 0 .1 0 .2.1l.1.1c.1.1.1.1.1.2v.3z"/>
            <path d="M433.6 394.1v-55.5l29.6-17.1V377z" class="st75"/>
            <path d="M459.1 346.6L437.6 359c-.6.3-1 0-1-.8v-16.6c0-.8.5-1.6 1-2l21.5-12.4c.6-.3 1 0 1 .8v16.6c0 .8-.4 1.7-1 2z" class="st0"/>
            <path d="M455.1 348.9c0-4.2-3-5.9-6.6-3.8-3.7 2.1-6.6 7.2-6.6 11.5l13.2-7.7zm-10.4-6.9c0-2.3 1.6-5.2 3.7-6.4s3.7-.2 3.7 2.1c0 2.3-1.6 5.2-3.7 6.4-2.1 1.2-3.7.3-3.7-2.1z" class="st76"/>
            <path d="M459.2 355l-21.9 12.7c-.4.2-.7 0-.7-.5V363c0-.5.3-1.1.7-1.4l21.9-12.7c.4-.2.7 0 .7.5v4.1c0 .6-.3 1.2-.7 1.5z" class="st0"/>
            <path d="M438.7 363.8c0-.7.5-1.5 1.1-1.9.6-.3 1.1-.1 1.1.6s-.5 1.5-1.1 1.9c-.6.3-1.1.1-1.1-.6zm2.8-1.7c0-.7.5-1.5 1.1-1.9s1.1-.1 1.1.6-.5 1.5-1.1 1.9c-.6.4-1.1.1-1.1-.6zm2.9-1.6c0-.7.5-1.5 1.1-1.9.6-.3 1.1-.1 1.1.6s-.5 1.5-1.1 1.9c-.7.4-1.1.1-1.1-.6zm2.8-1.6c0-.7.5-1.5 1.1-1.9s1.1-.1 1.1.6-.5 1.5-1.1 1.9-1.1.1-1.1-.6zm2.8-1.6c0-.7.5-1.5 1.1-1.9.6-.3 1.1-.1 1.1.6s-.5 1.5-1.1 1.9c-.6.4-1.1.1-1.1-.6zm2.8-1.6c0-.7.5-1.5 1.1-1.9.6-.3 1.1-.1 1.1.6s-.5 1.5-1.1 1.9-1.1.1-1.1-.6zm2.8-1.7c0-.7.5-1.5 1.1-1.9.6-.3 1.1-.1 1.1.6s-.5 1.5-1.1 1.9c-.6.4-1.1.1-1.1-.6z" class="st76"/>
            <path fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width=".391" d="M450.2 389.2l-3.7 2.1c-.4.2-.7 0-.7-.5s.3-1 .7-1.3l3.7-2.1c.4-.2.7 0 .7.5s-.3 1.1-.7 1.3z"/>
            <linearGradient id="SVGID_137_" x1="406.147" x2="451.171" y1="353.587" y2="358.978" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_137_)" d="M462.3 316.2l-.7.4-.2.1-32.2 18.6c-.4.2-.8.9-.8 1.4v63.8c-.3-.1-.4-.3-.4-.7v-63.1c0-.7.5-1.5 1-1.8l32.3-18.7c.1-.1.2-.1.3-.1h.3l.4.1z"/>
            <path d="M454.4 364c-1.3.7-2.7 1-3.8.9-.4 0-.7-.1-1.1-.3-.3.5-.7 1-1.1 1.5-1.1 1.5-2.5 2.7-3.8 3.5-.1 0-.1.1-.2.1v3.6c0 3.3 1.7 5.7 4.9 6.2 3.2-4.3 5.2-8.7 5.2-12.1v-3.6c0 .2 0 .2-.1.2zm-7.8 8.6c0-.5.4-1.1 1-1.4.5-.3 1-.2 1 .3s-.4 1.1-1 1.4c-.5.3-1 .2-1-.3zm4.5 1.8l-3.1 1.8v-1.5l3.1-1.8v1.5zm.4-3.7c-.5.3-1 .2-1-.3s.4-1.1 1-1.4c.5-.3 1-.2 1 .3 0 .4-.5 1-1 1.4z" class="st0"/>
            <linearGradient id="SVGID_138_" x1="403.626" x2="438.18" y1="332.377" y2="353.768" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5"/>
              <stop offset="1" stop-color="#71cbd5" stop-opacity="0"/>
            </linearGradient>
            <path fill="url(#SVGID_138_)" d="M464.8 317.6l-.3-.2c-.1 0-.1.1-.2.1L432 336.2c-.7.4-1.3 1.4-1.3 2.2v63.3l.3.2.4.2c-.1-.1-.2-.2-.2-.3v-63.4c0-.7.5-1.5 1-1.8l32.4-18.7c.2-.1.3-.1.5-.1h.2l-.5-.2z"/>
          </g>
          <g id="Slab_4_1_">
            <linearGradient id="SVGID_139_" x1="786.207" x2="848.419" y1="521.035" y2="521.035" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_139_)" d="M847 517.7l-26.7 15.4c-1.7 1-4.2 1.1-5.6.2l-27.5-15.9c-.7-.4-1-.9-1-1.5v-7.8h62.2v7.5c.1.8-.4 1.5-1.4 2.1z"/>
            <linearGradient id="SVGID_140_" x1="845.951" x2="800.613" y1="504.994" y2="510.032" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_140_)" d="M814.7 525.6l-27.5-15.9c-1.4-.8-1.3-2.3.4-3.3l26.7-15.4c1.7-1 4.2-1.1 5.6-.2l27.5 15.9c1.4.8 1.3 2.3-.4 3.3l-26.7 15.4c-1.6.9-4.2 1-5.6.2z"/>
            <linearGradient id="SVGID_141_" x1="836.612" x2="775.121" y1="506.032" y2="512.864" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_141_)" d="M817.5 490.2v.5c.9 0 1.7.2 2.2.5l27.5 15.9c.3.2.7.5.7 1s-.4 1-1.1 1.4l-26.7 15.4c-.8.5-1.9.7-3 .7-.9 0-1.7-.2-2.2-.5l-27.5-15.9c-.3-.2-.7-.5-.7-1s.4-1 1.1-1.4l26.7-15.4c.8-.5 1.9-.7 3-.7v-.5m0 0c-1.1 0-2.3.3-3.2.8l-26.7 15.4c-1.7 1-1.9 2.4-.4 3.3l27.5 15.9c.7.4 1.5.6 2.5.6 1.1 0 2.3-.3 3.2-.8l26.7-15.4c1.7-1 1.9-2.4.4-3.3L820 490.8c-.7-.4-1.6-.6-2.5-.6z"/>
            <path d="M796.4 518.8c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.5.3 1 .1 1-.6zm-2.6-1.4c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.6.3 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.6.3 1 0 1-.6z" class="st14"/>
            <g>
              <g class="st111">
                <path d="M831.8 505.6l-12.3 1.9 2-7.9-2 7.9 12.3-1.9m-16.1 4.1l-13.5 1.1 10.3 6 3.2-7.1m1.1-.1l-3.1 6.8 3.1-6.8" class="st75"/>
                <path d="M831.8 505.6l-10.3-6-2 7.9 12.3-1.9m-15 4l-3.1 6.8 3.1-6.8" class="st75"/>
                <path d="M831.1 506.3l-11.7 1.8-.4 1.3-2.2.2-3.1 6.8 17.4-10.1" class="st75"/>
                <path d="M820.4 499.8l-17.8 10.4L816 509h.6l.5-.1h1l.2-.6.1-.3v-.4l2-7.8" class="st75"/>
              </g>
            </g>
          </g>
          <g id="Slab_3_2_">
            <linearGradient id="SVGID_142_" x1="745.221" x2="807.433" y1="551.245" y2="551.245" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_142_)" d="M806.1 547.9l-26.7 15.4c-1.7 1-4.2 1.1-5.6.2l-27.5-15.9c-.7-.4-1-.9-1-1.5v-7.8h62.2v7.5c0 .8-.5 1.5-1.4 2.1z"/>
            <linearGradient id="SVGID_143_" x1="804.965" x2="759.627" y1="535.205" y2="540.242" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_143_)" d="M773.7 555.8l-27.5-15.9c-1.4-.8-1.3-2.3.4-3.3l26.7-15.4c1.7-1 4.2-1.1 5.6-.2l27.5 15.9c1.4.8 1.3 2.3-.4 3.3l-26.7 15.4c-1.6.9-4.1 1-5.6.2z"/>
            <linearGradient id="SVGID_144_" x1="795.794" x2="734.303" y1="536.242" y2="543.074" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_144_)" d="M776.7 520.4v.5c.9 0 1.7.2 2.2.5l27.5 15.9c.3.2.7.5.7 1s-.4 1-1.1 1.4l-26.7 15.4c-.8.5-1.9.7-3 .7-.9 0-1.7-.2-2.2-.5l-27.5-15.9c-.3-.2-.7-.5-.7-1s.4-1 1.1-1.4l26.7-15.4c.8-.5 1.9-.7 3-.7v-.5m0 0c-1.1 0-2.3.3-3.2.8l-26.7 15.4c-1.7 1-1.9 2.4-.4 3.3l27.5 15.9c.7.4 1.5.6 2.5.6 1.1 0 2.3-.3 3.2-.8l26.7-15.4c1.7-1 1.9-2.4.4-3.3L779.1 521c-.6-.4-1.5-.6-2.4-.6z"/>
            <g class="st111">
              <path d="M774.2 539l-1.6-.9c-.3.4-.2.7.2 1 .3.1.7.2 1.4-.1m1.7-.8l-1.9-1.1c-.2.1-.3.2-.5.3-.1 0-.1.1-.2.1l1.9 1.1.7-.4m3.4 2l-2-1.1-.7.3 2 1.2c.1-.1.3-.2.4-.2.2-.2.3-.2.3-.2m.6-1.7c-.3-.2-.8-.3-1.6 0l1.7 1c.5-.4.3-.8-.1-1" class="st75"/>
              <path d="M775.1 536.7l1.8 1c2-.9 3.5-.8 4.6-.2 1 .6 1.4 1.7 0 2.9l1 .6-.9.5-.9-.5-.2.1c-.2.1-.3.2-.4.2l.9.5-.9.5-1.1-.6c-1.9.7-3.7.7-4.7.1l-.1-.1c-.1-.1-.1-.2 0-.3l1.2-.7c.2-.1.3-.1.5 0 .5.2 1 .3 1.6.1l-1.8-1.1c-1.7.8-3.3.9-4.4.2-1.1-.7-1.4-1.7-.1-2.9l-1-.6.9-.5.9.5.2-.1c.1-.1.3-.2.4-.2l-.9-.5.9-.5 1.1.6c1.8-.7 3.5-.6 4.5 0 .1.1.1.2 0 .3l-1.2.7c-.1.1-.2.1-.4 0-.5 0-.9-.1-1.5 0m9.2-2.5c-4.4-2.5-11.5-2.5-15.9 0s-4.4 6.7.1 9.2c4.4 2.5 11.5 2.5 15.9 0 4.3-2.5 4.3-6.6-.1-9.2" class="st75"/>
            </g>
          </g>
          <g id="Slab_2_2_">
            <linearGradient id="SVGID_145_" x1="704.235" x2="766.447" y1="549.655" y2="549.655" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_145_)" d="M765.1 546.3l-26.7 15.4c-1.7 1-4.2 1.1-5.6.2L705.3 546c-.7-.4-1-.9-1-1.5v-7.8h62.2v7.5c0 .8-.5 1.5-1.4 2.1z"/>
            <linearGradient id="SVGID_146_" x1="763.979" x2="718.641" y1="533.615" y2="538.652" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_146_)" d="M732.7 554.2l-27.5-15.9c-1.4-.8-1.3-2.3.4-3.3l26.7-15.4c1.7-1 4.2-1.1 5.6-.2l27.5 15.9c1.4.8 1.3 2.3-.4 3.3L738.4 554c-1.7.9-4.2 1-5.7.2z"/>
            <linearGradient id="SVGID_147_" x1="754.64" x2="693.149" y1="534.653" y2="541.485" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_147_)" d="M735.5 518.8v.5c.9 0 1.7.2 2.2.5l27.5 15.9c.3.2.7.5.7 1s-.4 1-1.1 1.4l-26.7 15.4c-.8.5-1.9.7-3 .7-.9 0-1.7-.2-2.2-.5l-27.5-15.9c-.3-.2-.7-.5-.7-1s.4-1 1.1-1.4l26.7-15.4c.8-.5 1.9-.7 3-.7v-.5m0 0c-1.1 0-2.3.3-3.2.8L705.6 535c-1.7 1-1.9 2.4-.4 3.3l27.5 15.9c.7.4 1.5.6 2.5.6 1.1 0 2.3-.3 3.2-.8l26.7-15.4c1.7-1 1.9-2.4.4-3.3L738 519.4c-.7-.4-1.6-.6-2.5-.6z"/>
            <g>
              <g class="st111">
                <path d="M730.8 536.3c-1.4-.8-1.4-2.1 0-3 1.4-.8 3.7-.8 5.1 0 1.4.8 1.4 2.2 0 3-1.4.9-3.7.8-5.1 0m9.6-5.5c-.1-.1-.2-.1-.3-.2-3.7-2-9.6-1.9-13.3 0-.1.1-.2.1-.3.2s-.2.1-.3.2c-3.4 2.2-3.4 5.5 0 7.7.1.1.2.1.3.2 1.7 1 3.6 1.3 6.1 1.7l12.6 1.2c.2 0 .4-.1.4-.2l-2.2-7.3c-.6-1.2-1.3-2.5-3-3.5" class="st75"/>
              </g>
            </g>
            <g>
              <path d="M714 547.1c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7c.6.3 1 0 1-.6zm-2.6-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6 0 .6.5 1.4 1 1.7.6.3 1 0 1-.6zm-2.5-1.5c0-.6-.5-1.4-1-1.7-.6-.3-1-.1-1 .6s.5 1.4 1 1.7 1 0 1-.6z" class="st14"/>
            </g>
          </g>
          <g id="Slab_1_2_">
            <linearGradient id="SVGID_148_" x1="663.249" x2="725.461" y1="592.025" y2="592.025" gradientUnits="userSpaceOnUse">
              <stop offset=".391" stop-color="#143da1"/>
              <stop offset=".61" stop-color="#00074d"/>
              <stop offset="1" stop-color="#00074d"/>
            </linearGradient>
            <path fill="url(#SVGID_148_)" d="M724.1 588.7l-26.7 15.4c-1.7 1-4.2 1.1-5.6.2l-27.5-15.9c-.7-.4-1-.9-1-1.5v-7.8h62.2v7.5c0 .8-.4 1.5-1.4 2.1z"/>
            <linearGradient id="SVGID_149_" x1="722.993" x2="677.655" y1="575.984" y2="581.022" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#00074d"/>
              <stop offset=".464" stop-color="#143da1"/>
              <stop offset=".994" stop-color="#143da1"/>
            </linearGradient>
            <path fill="url(#SVGID_149_)" d="M691.7 596.6l-27.5-15.9c-1.4-.8-1.3-2.3.4-3.3l26.7-15.4c1.7-1 4.2-1.1 5.6-.2l27.5 15.9c1.4.8 1.3 2.3-.4 3.3l-26.7 15.4c-1.6.9-4.1 1-5.6.2z"/>
            <linearGradient id="SVGID_150_" x1="713.654" x2="652.163" y1="577.022" y2="583.854" gradientUnits="userSpaceOnUse">
              <stop offset="0" stop-color="#71cbd5" stop-opacity="0"/>
              <stop offset="1" stop-color="#71cbd5"/>
            </linearGradient>
            <path fill="url(#SVGID_150_)" d="M694.5 561.2v.5c.9 0 1.7.2 2.2.5l27.5 15.9c.3.2.7.5.7 1s-.4 1-1.1 1.4l-26.7 15.4c-.8.5-1.9.7-3 .7-.9 0-1.7-.2-2.2-.5l-27.5-15.9c-.3-.2-.7-.5-.7-1s.4-1 1.1-1.4l26.7-15.4c.8-.5 1.9-.7 3-.7v-.5m0 0c-1.1 0-2.3.3-3.2.8l-26.7 15.4c-1.7 1-1.9 2.4-.4 3.3l27.5 15.9c.7.4 1.5.6 2.5.6 1.1 0 2.3-.3 3.2-.8l26.7-15.4c1.7-1 1.9-2.4.4-3.3L697 561.8c-.7-.4-1.6-.6-2.5-.6z"/>
            <g>
              <g class="st111">
                <path d="M689.3 579c-.1 0-.1-.1-.2-.1-1.4-.9-1.2-2.3.3-3.1 1.5-.8 3.8-.7 5.2.1.1 0 .1.1.2.1 1.4.9 1.3 2.3-.3 3.1-1.4.8-3.7.7-5.2-.1m13-5.4c-2.2-1.3-5-2-7.9-2.1-3.1-.1-6.3.5-8.8 1.8-2.5 1.3-3.9 3.1-4.1 4.9-.2 1.8.8 3.7 3.1 5.1.2.1.3.2.5.3 2.8 1.6 6.8 2.3 10.5 2l-.8-.5c-1.3-.8-1.8-1.9-1.7-2.9.1-1 .9-2 2.3-2.7 2.7-1.4 6.8-1.3 9.4.2.1.1.2.1.3.2l.6.4c.1-.4.2-.7.3-1.1.2-1.8-.8-3.6-3.1-5.1-.3-.3-.5-.4-.6-.5" class="st75"/>
              </g>
            </g>
            <g>
              <path d="M671.8 588.7c0-.6-.3-1.3-.8-1.6-.5-.3-.9 0-.9.6s.3 1.3.8 1.6c.5.3.9 0 .9-.6zm-2.2-1.3c0-.6-.3-1.3-.8-1.6-.5-.3-.9 0-.9.6s.3 1.3.8 1.6c.5.3.9.1.9-.6zm-2.1-1.2c0-.6-.3-1.3-.8-1.6s-.9 0-.9.6.3 1.3.8 1.6c.4.3.8 0 .9-.6z" class="st14"/>
            </g>
          </g>
        </svg>
      </div>
      <!-- What Section -->
      <section id="what" class="what">
        <h2 class="what__header">Privacy. Security. Anonymity.</h2>
        <p class="tagline"></p>
        <div class="what__row">
          <p class="what__description">
            Today you're often left with one of two choices: Sign up and give up your personal information or be refused access to the service. Since so much of our daily life is online this presents a dilema. With Klon, you can create profiles called <em>"clones"</em> allowing you to keep your personal information private while gaining access to the services you need.
          </p>
        </div>
      </section>
      <!-- Why Section -->
      <section id="why" class="why">
        <h2 class="header">Why</h2>
        <p class="tagline">You should use Klon</p>
        <div class="why__row">
          <div class="why__column">
            <img src="img/alert.svg" alt="Alert" height="120" width="120">
            <h3 class="why__h3 why__h3--alert">Without Klon</h3>
            <p class="why__text why__text--alert">We all manage many accounts on the internet. When you use the same information for every account they become easily connected to you.</p>
            <h3 class="why__h3 why__h3--alert">The Problem</h3>
            <p class="why__text why__text--alert">If one of these accounts gets hacked all of your other accounts and their associated data are at risk of being compromised. This can include your full name, street address, email address, passwords, etc... We call this the domino effect.</p>
          </div>
          <!-- Maybe an image showing this here -->
          <!-- <div class="why__column">
            <img src="" alt="">
          </div> -->
          <div class="why__column">
            <img src="img/checkmark.svg" alt="Error" height="120" width="120">
            <h3 class="why__h3">With Klon</h3>
            <p class="why__text">You create a "clone" for each new sign up. Now there is no connection between you and your accounts.</p>
            <p class="why__text">If one of these "clones" becomes compromised all of your other accounts are still safe! Best of all your personal information is not floating around on the internet.</p>
          </div>
          <!-- <div class="why__column">
            <img class="why__image" alt="Fingerprint" src="img/fingerprint.svg" height="" width="">
            <p class="why__text">
              Minimize your digital fingerprint
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="Cake" src="img/candy.svg" height="" width="">
            <p class="why__text">
              Makes signing up and signing in a piece of cake
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="YOUR personal information" src="img/error.svg" height="" width="">
            <p class="why__text">
              Your personal information stays <em>your</em> personal information in the event of a website hack
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="Clean Inbox" src="img/paper-plane.svg" height="" width="">
            <p class="why__text">
              Eliminate spam from reaching your inbox
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="" src="img/key.svg" height="" width="">
            <p class="why__text">
              Only you hold the keys to your data
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="" src="img/password.svg" height="" width="">
            <p class="why__text">
              Seriously secure passwords
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="" src="img/shield.svg" height="" width="">
            <p class="why__text">
              All of your identities are stored with AES-256 encryption
            </p>
          </div>
          <div class="why__column">
            <img class="why__image" alt="" src="img/hazard.svg" height="" width="">
            <p class="why__text">
              Eliminate the fallout from data breaches
            </p>
          </div> -->
        </div>
      </section>
      <!-- How Section -->
      <section id="how" class="how">
        <h2 class="header">How</h2>
        <p class="tagline">To use Klon</p>
        <ul class="how__list">
          <li class="how__step how__step--1">
            <div class="how__frame">
              <h3 class="how__stepHeader">1. Download</h3>
              <p class="how__dark">Download the extension by clicking the button below.</p>
              <a href="./download" class="button__download">Download</a>
            </div>
          </li>
          <!-- Step 2 | Sign Up -->
          <li class="how__step how__step--2">
            <svg class="seperator seperator--createAccount" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      				<path d="M0 100 C40 0 60 0 100 100 Z"></path>
      			</svg>
            <div class="how__frame">
              <h3 class="how__stepHeader">2. Create an Account</h3>
              <ul class="how__list">
                <li class="how__listItem">Sign up within the extension. <br><small>Note: Your password must contain at least 8 characters</small></li>
                <li class="how__listItem">Make sure to create a really strong password. Your password will be used as part of the encryption key that protects every subsequently generated identity. <strong>If you forget your password there is no way to recover your account!</strong></li>
              </ul>
              <video class="how__video how__video--signup" controls id="vidSignUp">
                <source src="video/signup.mp4" type="video/mp4">
                <source src="video/signup.webm" type="video/webm">
                <p>Your browser doesn't support HTML5 video. Here is a <a href="video/signup.mp4">link to the video</a> instead.</p>
              </video>
            </div>
          </li>
          <!-- Step 3 | Generate an Identity -->
          <li class="how__step how__step--3">
            <svg class="seperator seperator--generateIdentity" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      				<path d="M0 100 C40 0 60 0 100 100 Z"></path>
      			</svg>
            <div class="how__frame">
              <h3 class="how__stepHeader">3. Create a Clone</h3>
              <p class="how__light">Visit a website you would like to sign up for. Now click <em>Create</em> to create a new clone.</p>
              <p class="how__light">Once you have a clone you would like to use, click <em>Fill</em>.</p>
              <p class="how__light">This will allow you to fill the form with the data generated by Klon.</p>
              <p class="how__light">If the website requires email validation, we've got you covered. Just visit the <em>Mail</em> tab within Klon.</p>
              <video class="how__video how__video--generate" controls id="vidGenerate">
                <source src="video/demo.mp4" type="video/mp4">
                <source src="video/demo.webm" type="video/webm">
                <p>Your browser doesn't support HTML5 video. Here is a <a href="video/demo.mp4">link to the video</a> instead.</p>
              </video>
            </div>
          </li>
          <!-- Step 4 | Autofill Magic -->
          <li class="how__step how__step--4">
            <svg class="seperator seperator--autoFill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      				<path d="M0 100 C40 0 60 0 100 100 Z"></path>
      			</svg>
            <div class="how__frame">
              <h3 class="how__stepHeader">4. Sign In</h3>
              <p class="how__light">Now you can login with the click of a button with Klon's built-in <em>Fill Magic</em>!</p>
              <p class="how__light">Simply visit the login page of the website you want to login to and click the Klon icon in the input field.</p>
              <video class="how__video how__video--fill" controls id="vidFill">
                <source src="video/fill.mp4" type="video/mp4">
                <source src="video/fill.webm" type="video/webm">
                <p>Your browser doesn't support HTML5 video. Here is a <a href="video/fill.mp4">link to the video</a> instead.</p>
              </video>
            </div>
          </li>
        </ul>
      </section>
      <!-- Get Started Today -->
      <section class="callToAction">
        <h3 class="callToAction__header">Get started today!</h3>
        <div class="callToAction__body">
          <p class="callToAction__tagline">
            You can try Klon Premium for 30 days absolutely free!
            <br>Right now we support <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://brave.com" target="_blank">Brave</a>, and <a href="https://firefox.com" target="_blank">Firefox</a> browsers. We are currently working to make our extension compatible with more browsers!
          </p>
          <!-- Browsers -->
          <img class="callToAction__supported" src="img/chrome.svg" alt="Supported Browsers" height="100" width="100" aria-label="Chrome Supported" title="Works with Chrome!">
          <img class="callToAction__supported" src="img/brave.svg" alt="Supported Browsers" height="100" width="100" aria-label="Brave supported" title="Works with Brave Browser!">
          <img class="callToAction__supported" src="img/firefox.svg" alt="Supported Browsers" height="100" width="100" aria-label="Firefox Supported" title="Works with Firefox!">
          <img class="callToAction__unsupported" src="img/ie.svg" alt="Supported Browsers" height="100" width="100" aria-label="IE Unsupported" title="IE/Edge not supported">
          <img class="callToAction__unsupported" src="img/opera.svg" alt="Supported Browsers" height="100" width="100" aria-label="Opera Unsupported" title="Opera not supported">
          <img class="callToAction__unsupported" src="img/safari.svg" alt="Supported Browsers" height="100" width="100" aria-label="Safari Unsupported" title="Safari not supported">
        </div>
        <a class="callToAction__button" href="./download">Download Now</a>
      </section>
      <script asyc src="./js/animation.js"></script>
      <script type="text/javascript">
        window.onload = function() {
          let why = document.querySelector("#why");
          let aBtnLearnMore = document.querySelector("#aLearnMore");
          let btnLearnMore  = document.querySelector("#btnLearnMore");
          aBtnLearnMore.addEventListener("click", function() {
            why.scrollIntoView({
              behavior : 'smooth'
            });
          });
          btnLearnMore.addEventListener("click", function() {
            why.scrollIntoView({
              behavior : 'smooth'
            });
          });
        }
      </script>
      <!-- Footer -->
      <?php include_once('footer.php'); ?>
