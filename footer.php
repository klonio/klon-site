<!-- Footer -->
    <footer class="footer">
      <div class="footer__section">
        <nav class="footer__nav">
          <ul class="footer__navList footer__navList--left">
            <li class="footer__navListItem"><a id="top">Back to Top</a></li>
            <li class="footer__navListItem"><a href="./terms" id="tos">Terms of Service</a></li>
            <li class="footer__navListItem"><a href="./privacy" id="policy">Privacy Policy</a></li>
          </ul>
        </nav>
      </div>
      <div class="footer__section">
        <ul class="footer__social">
          <li class="footer__socialItem">
            <a href="https://facebook.com/KlonPrivacy" target="_blank">
              <!-- Facebook -->
              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="#fff" d="M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0zm3 8h-1.35c-.538 0-.65.221-.65.778V10h2l-.209 2H13v7h-3v-7H8v-2h2V7.692C10 5.923 10.931 5 13.029 5H15v3z"/></svg>
            </a>
          </li>
          <li class="footer__socialItem">
            <a href="https://twitter.com/KlonPrivacy" target="_blank">
              <!-- Twitter -->
              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="#fff" d="M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0zm6.066 9.645c.183 4.04-2.83 8.544-8.164 8.544A8.127 8.127 0 0 1 5.5 16.898a5.778 5.778 0 0 0 4.252-1.189 2.879 2.879 0 0 1-2.684-1.995 2.88 2.88 0 0 0 1.298-.049c-1.381-.278-2.335-1.522-2.304-2.853.388.215.83.344 1.301.359a2.877 2.877 0 0 1-.889-3.835 8.153 8.153 0 0 0 5.92 3.001 2.876 2.876 0 0 1 4.895-2.62 5.73 5.73 0 0 0 1.824-.697 2.884 2.884 0 0 1-1.263 1.589 5.73 5.73 0 0 0 1.649-.453 5.765 5.765 0 0 1-1.433 1.489z"/></svg>
            </a>
          </li>
          <li class="footer__socialItem">
            <a href="https://medium.com/@klonprivacy" target="_blank">
              <!-- Medium -->
              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill-rule="evenodd" viewBox="0 0 24 24"><path fill="#fff" d="M12 0c-6.626 0-12 5.372-12 12 0 6.627 5.374 12 12 12 6.627 0 12-5.373 12-12 0-6.628-5.373-12-12-12zm6.974 17.557v-.235l-1.092-1.072c-.096-.073-.144-.194-.124-.313v-7.874c-.02-.119.028-.24.124-.313l1.118-1.072v-.235h-3.869l-2.758 6.88-3.138-6.88h-4.059v.235l1.308 1.575c.128.115.194.285.176.457v6.188c.038.223-.032.451-.189.614l-1.471 1.784v.235h4.17v-.235l-1.471-1.784c-.158-.163-.233-.389-.202-.614v-5.352l3.66 7.985h.425l3.143-7.985v6.365c0 .17 0 .202-.111.313l-1.13 1.098v.235h5.49z"/></svg>
            </a>
          </li>
          <!-- <li class="footer__socialItem"> -->
            <!-- <a href="#"> -->
              <!-- YouTube -->
              <!-- <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="#fff" d="M10.918 13.933h.706v3.795h-.706v-.419c-.13.154-.266.272-.405.353-.381.218-.902.213-.902-.557v-3.172h.705v2.909c0 .153.037.256.188.256.138 0 .329-.176.415-.284v-2.881zm.642-4.181c.2 0 .311-.16.311-.377V7.521c0-.223-.098-.38-.324-.38-.208 0-.309.161-.309.38v1.854c-.001.21.117.377.322.377zm-1.941 2.831H7.18v.747h.823v4.398h.795V13.33h.821v-.747zm4.721 2.253v2.105c0 .47-.176.834-.645.834-.259 0-.474-.094-.671-.34v.292h-.712v-5.145h.712v1.656c.16-.194.375-.354.628-.354.517.001.688.437.688.952zm-.727.043c0-.128-.024-.225-.075-.292-.086-.113-.244-.125-.367-.062l-.146.116v2.365l.167.134c.115.058.283.062.361-.039.04-.054.061-.141.061-.262v-1.96zM24 12c0 6.627-5.373 12-12 12S0 18.627 0 12 5.373 0 12 0s12 5.373 12 12zM13.254 9.749c0 .394.12.712.519.712.224 0 .534-.117.855-.498v.44h.741V6.417h-.741v3.025c-.09.113-.291.299-.436.299-.159 0-.197-.108-.197-.269V6.417h-.741v3.332zm-2.779-2.294v1.954c0 .703.367 1.068 1.085 1.068.597 0 1.065-.399 1.065-1.068V7.455a1.04 1.04 0 0 0-1.065-1.071c-.652 0-1.085.432-1.085 1.071zM7.714 5l.993 3.211v2.191h.835V8.211L10.513 5h-.848L9.13 7.16 8.555 5h-.841zm10.119 10.208c-.013-2.605-.204-3.602-1.848-3.714-1.518-.104-6.455-.103-7.971 0-1.642.112-1.835 1.104-1.848 3.714.013 2.606.204 3.602 1.848 3.715 1.516.103 6.453.103 7.971 0 1.643-.113 1.835-1.104 1.848-3.715zm-.885-.255v.966h-1.35v.716c0 .285.024.531.308.531.298 0 .315-.2.315-.531v-.264h.727v.285c0 .731-.313 1.174-1.057 1.174-.676 0-1.019-.491-1.019-1.174v-1.704c0-.659.435-1.116 1.071-1.116.678.001 1.005.431 1.005 1.117zm-.726-.007c0-.256-.054-.445-.309-.445-.261 0-.314.184-.314.445v.385h.623v-.385z"/></svg> -->
            <!-- </a> -->
          <!-- </li> -->
          <!-- <li class="social__item"> -->
            <!-- <a href="#"> -->
              <!-- Mastodon -->
            <!-- </a> -->
          <!-- </li> -->
        </ul>
        <small class="footer__copyright">&copy; Copyright 2019</small>
      </div>
      <div class="footer__section">
        <nav class="footer__nav">
          <ul class="footer__navList footer__navList--right">
            <li class="footer__navListItem"><a href="./download">Download</a></li>
            <li class="footer__navListItem"><a href="./faq">FAQ</a></li>
            <li class="footer__navListItem"><a href="./roadmap">Roadmap</a></li>
            <li class="footer__navListItem"><a href="./pricing">Pricing</a></li>
            <li class="footer__navListItem"><a href="./tech">Technical Details</a></li>
          </ul>
        </nav>
      </div>
    </footer>
  </div>
  <script type="text/javascript">
  try {
    let backToTop = document.querySelector("#top");
    let theTop    = document.querySelector("#theTop") || false;
    if (theTop) {
      backToTop.addEventListener("click", function(e) {
        e.preventDefault();
        theTop.scrollIntoView({
          behavior : 'smooth'
        });
      });
    } else {
    }
  } catch (e) {
    console.log(e.message);
  }
  </script>
  <script type="text/javascript" src="./js/main-dist.js"></script>
  <script>var privalytics_id="PL-DC72AE";</script>
  <script async src="https://cdn.privalytics.io/privalytics.js"></script>
  </body>
</html>
