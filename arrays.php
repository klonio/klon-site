<?php
  $mainNav = array(
    array(
      'slug'   => "faq.php",
      'title'  => "FAQ",
      'search' => "faq"
    ),
    array(
      'slug'   => "about.php",
      'title'  => "About",
      'search' => "about"
    ),
    array(
      'slug'   => "roadmap.php",
      'title'  => "Roadmap",
      'search' => "roadmap"
    ),
    array(
      'slug'   => "pricing.php",
      'title'  => "Pricing",
      'search' => "pricing"
    ),
    array(
      'slug'   => "download.php",
      'title'  => "Download",
      'search' => "download"
    ),
    array(
      'slug'   => "tech.php",
      'title'  => "Technical",
      'search' => "tech"
    )
  );

  $pages = array(
    array(
      'slug'   => 'tech.php',
      'title'  => 'Tech',
      'search' => 'tech'
    ),
    array(
      'slug'   => "faq.php",
      'title'  => "FAQ",
      'search' => "faq"
    ),
    array(
      'slug'   => "about.php",
      'title'  => "About",
      'search' => "about"
    ),
    array(
      'slug'   => "roadmap.php",
      'title'  => "Roadmap",
      'search' => "roadmap"
    ),
    array(
      'slug'   => "pricing.php",
      'title'  => "Pricing",
      'search' => "pricing"
    ),
    array(
      'slug'   => "download.php",
      'title'  => "Download",
      'search' => "download"
    ),
    array(
      'slug'   => "login.php",
      'title'  => "Login",
      'search' => "login"
    ),
    array(
      'slug'   => "terms.php",
      'title'  => "Terms",
      'search' => "terms"
    ),
    array(
      'slug'   => "privacy.php",
      'title'  => "Privacy",
      'search' => "privacy"
    )
  );
?>
