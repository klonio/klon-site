<section class="download">
  <h2 class="download__heading">Download</h2>
  <p class="download__text">
    Select your browser from the options below to download Klon Privacy Extension.
  </p>
  <div class="download__table">
    <div class="download__browser">
      <div class="download__iconGroup">
        <img class="download__icon" src="./img/chrome.svg" alt="Chrome Browser" height="" width="">
        <img class="download__icon" src="./img/brave.svg" alt="Brave Browser" height="" width="">
      </div>
      <div class="download__browserTitle">
        <h3>Chrome / Brave / Chromium</h3>
      </div>
      <a href="https://chrome.google.com/webstore/detail/klon-privacy-extension/hikhnlcnccbefjgbmanmooencjghcklg" class="download__button" target="_blank">Download Now</a>
    </div>
    <div class="download__browser">
      <div class="download__iconGroup">
        <img class="download__icon" src="./img/firefox.svg" alt="Firefox Browser" height="" width="">
      </div>
      <div class="download__browserTitle">
        <h3>Firefox</h3>
      </div>
      <a href="https://addons.mozilla.org/en-US/firefox/addon/klon-privacy-extension/" class="download__button">Download Now</a>
    </div>
  </div>
</section>
